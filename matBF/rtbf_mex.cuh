/**
 @file utils/rtbf_mex.cuh
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2022-05-01

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef RTBF_MEX_CUH_
#define RTBF_MEX_CUH_
#include <algorithm>
#include <exception>
#include <fstream>
#include <iostream>
#include <map>
#include <string>
#include <typeinfo>
#include <variant>
#include <vector>

#include "Bmode.cuh"
#include "ChannelSum.cuh"
#include "Demodulate.cuh"
#include "Focus.cuh"
#include "FocusLUT.cuh"
#include "Hilbert.cuh"
#include "InputLoader.cuh"
#include "Refocus.cuh"
#include "SVDFilter.cuh"
#include "ScanConvert2D.cuh"
#include "VSXFormatter.cuh"
#include "interp1d.cuh"
#include "rtbf_base.cuh"
#include "rtbf_help.cuh"
#include "type_inference.h"
#include "type_names_mat.h"

// Define namespace aliases to simplify the code
using namespace rtbf;
namespace mmex = matlab::mex;
namespace meng = matlab::engine;
namespace mdat = matlab::data;

// MATLAB uses std::complex<T>. CUDA uses cuda::std::complex<T>.
// Define a few typedefs to simplify the code
typedef std::complex<short> sshort;
typedef std::complex<float> sfloat;
typedef cuda::std::complex<short> cshort;
typedef cuda::std::complex<float> cfloat;

// Create a variant containing a type-safe union of all possible Tensor types.
using TensorType = std::variant<
    std::shared_ptr<Tensor<short>>, std::shared_ptr<Tensor<float>>,
    std::shared_ptr<Tensor<cshort>>, std::shared_ptr<Tensor<cfloat>>,
    std::shared_ptr<Tensor<sshort>>, std::shared_ptr<Tensor<sfloat>>>;

std::vector<size_t> getDimensions(TensorType t) {
  return std::visit([](auto &&p) { return p->getDimensions(); }, t);
}

// Create a variant containing a type-safe union of all possible
// Operator types.
using OperatorType =
    std::variant<std::shared_ptr<VSXFormatter<short, cshort>>,
                 std::shared_ptr<VSXFormatter<short, cfloat>>,
                 std::shared_ptr<InputLoader<short, short>>,
                 std::shared_ptr<InputLoader<float, float>>,
                 std::shared_ptr<InputLoader<cshort, cshort>>,
                 std::shared_ptr<InputLoader<cfloat, cfloat>>,
                 std::shared_ptr<Demodulate<short, cfloat>>,
                 std::shared_ptr<Demodulate<float, cfloat>>,
                 std::shared_ptr<Demodulate<cshort, cfloat>>,
                 std::shared_ptr<Demodulate<cfloat, cfloat>>,
                 std::shared_ptr<Decimate<short, short>>,
                 std::shared_ptr<Decimate<cshort, cshort>>,
                 std::shared_ptr<Decimate<float, float>>,
                 std::shared_ptr<Decimate<cfloat, cfloat>>,
                 std::shared_ptr<Hilbert<short, cshort>>,
                 std::shared_ptr<Hilbert<float, cshort>>,
                 std::shared_ptr<Hilbert<short, cfloat>>,
                 std::shared_ptr<Hilbert<float, cfloat>>,
                 std::shared_ptr<Focus<float, float>>,
                 std::shared_ptr<Focus<cfloat, cfloat>>,
                 std::shared_ptr<FocusLUT<float, float>>,
                 std::shared_ptr<FocusLUT<cfloat, cfloat>>,
                 std::shared_ptr<ChannelSum<short, short>>,
                 std::shared_ptr<ChannelSum<cshort, cshort>>,
                 std::shared_ptr<ChannelSum<float, float>>,
                 std::shared_ptr<ChannelSum<cfloat, cfloat>>,
                 std::shared_ptr<Bmode<cshort, float>>,
                 std::shared_ptr<Bmode<cfloat, float>>,
                 std::shared_ptr<Refocus<cfloat, cfloat>>,
                 std::shared_ptr<ScanConvert2D<float, float>>,
                 std::shared_ptr<ScanConvert2D<cfloat, cfloat>>,
                 std::shared_ptr<SVDFilter<float, float>>,
                 std::shared_ptr<SVDFilter<cfloat, cfloat>>>;

struct GraphData {
  std::string name;                         ///< Name of the graph
  std::map<std::string, TensorType> tns;    ///< Map of names to Tensors
  std::map<std::string, OperatorType> ops;  ///< Map of names to Operators
  std::vector<std::string> op_names;  ///< Vector of Operator names in order
  std::vector<std::function<void()>> fns;  ///< Vector of executable functions
  bool vsx_input = false;       ///< Whether the input will be VSX data or not
  bool focus_baseband = false;  ///< Whether baseband focusing will be used
  int vsx_dsf = 1;              ///< Downsampling factor for VSXFormatter
  std::string lastTensor;       ///< Most recent Tensor's name for convenience
  cudaStream_t stream;          ///< CUDA stream
  bool verbose = false;         ///< Verbosity
  GraphData() { CCE(cudaStreamCreate(&stream)); }
  virtual ~GraphData() {
    CCE(cudaStreamDestroy(stream));
    if (verbose) std::cout << "Clearing graph \"" << name << "\"." << std::endl;
  }
};

/**	@brief MexFunction subclass with built-in utilities for rtbf.

rtbf_mex provides a unified interface between MATLAB and rtbf, allowing MATLAB
users to utilize any of a fixed set of Operators. This class inherits from the
rtbf_base class, which contains numerous useful functions that simplify loading
Tensors and other data from MATLAB Arrays, as well as printing messages,
warnings, and errors, and copying data arrays to and from the GPU. The possible
Operators are determined by the std::variant OperatorType; as more Operators are
included, this variant must be expanded and updated.

*/

class MexFunction : public rtbf_base {
 private:
  int gpuID = 0;                               ///< Device ID
  std::map<std::string, GraphData> graph_map;  ///< Map of all graphs' data
  bool verbose = false;                        ///< Output to MATLAB

  /// @brief Helper function to add a new Operator to graph
  void addOperatorHelper(GraphData &graph, const mdat::StructArray &S,
                         std::string &name, TensorType &input,
                         std::string &out_name);

 public:
  // Constructor
  MexFunction() {  // Place default values in the constructor
    matlabPtr = getEngine();
    gpuID = getFirstAvailableGPU();
    setLogger(logFile, spdlog::level::warn);
    slog->flush_on(spdlog::level::warn);
    // setLogger(logFile, spdlog::level::debug);
    // slog->flush_on(spdlog::level::debug);
  }
  // Destructor, called on clear mex or MATLAB exit
  ~MexFunction() {}
  // Main function to be executed
  void operator()(mmex::ArgumentList outputs, mmex::ArgumentList inputs) {
    try {
      int nargin = inputs.size();
      // The first input should be RF data of data type 'single'.
      if (nargin == 0)
        printErr("Not enough inputs. See rtbf_mex('help') for instructions.");

      int start_idx = 0;
      // First things first: Determine which graph to work on.
      std::string graph_name = "default";
      if (inputs[0].getType() == mdat::ArrayType::CHAR) {
        graph_name = static_cast<mdat::CharArray>(inputs[0]).toAscii();
        start_idx = 1;
      }

      // In the special case where graph_name is "help", output help information
      if (graph_name.compare("help") == 0) {
        std::string opt = "";
        if (nargin == 2 && inputs[1].getType() == mdat::ArrayType::CHAR)
          opt = static_cast<mdat::CharArray>(inputs[1]).toAscii();
        printMsg(getHelpMessage(opt));
        return;
      }

      // Must have at least one input besides the graph name.
      if (nargin == start_idx) printErr("Not enough inputs.");

      // In the special case where graph_name is "settings", set global
      // settings.
      if (graph_name.compare("settings") == 0) {
        mdat::StructArray S = inputs[start_idx];
        int tmp = getOptScalar(S, "gpuID", -100);
        if (tmp != -100) gpuID = tmp;
        verbose = getOptScalar(S, "verbose", 1);
        return;
      }

      // First, do some error checking to make sure the graph exists and does
      // not need to be overwritten.
      auto &gr = graph_map[graph_name];
      // On the first run, operators and/or execution functions must be
      // provided
      int nops = nargin - start_idx - 1;
      if (gr.ops.empty() && nops == 0) printWrn("No Operators specified.");
      if (gr.fns.empty() && nops == 0)
        printWrn("Execution queue is empty. Make sure to initialize.");
      // If operators are provided to an existing graph, reset the graph
      if (nops > 0 && !gr.lastTensor.empty()) {
        if (verbose)
          printMsg("Clearing graph \"{}\" and reinitializing.", gr.name);
        graph_map.erase(graph_name);  // Erase graph
      }

      // Grab the GraphData struct for the requested graph_name
      auto &graph = graph_map[graph_name];  // Get a reference to graph
      graph.name = graph_name;  // Graph's name matches its name in map
      graph.verbose = verbose;

      // The first task is always to create the input data Tensor
      if (graph.tns.empty()) {  // If graph is empty
        if (verbose) printMsg("Using graph \"{}\".", graph.name);

        mdat::StructArray S = inputs[start_idx + 1];
        checkFieldsExist(S, {"operator"});
        if (getReqString(S, "operator").compare("VSXFormatter") == 0) {
          // If using VSXFormatter, peek at the expected input dimensions
          graph.vsx_input = true;
          size_t nsamps = getReqScalar(S, "nsamps");
          size_t nxmits = getReqScalar(S, "nxmits");
          size_t nchans = getReqScalar(S, "nchans");
          size_t nframes = getOptScalar(S, "nframes", 1);
          size_t npulses = getOptScalar(S, "npulses", 1);
          std::vector<size_t> idims = {nsamps * npulses * nxmits, nchans,
                                       nframes};
          addInputLoaderToGraph(graph, inputs[start_idx], idims);
        } else {
          graph.vsx_input = false;
          addInputLoaderToGraph(graph, inputs[start_idx]);
        }
      }

      for (int i = start_idx + 1; i < nargin; i++) {
        if (inputs[i].getType() != mdat::ArrayType::STRUCT)
          printErr("All trailing arguments must be Operator structs.");
        mdat::StructArray S = inputs[i];
        checkFieldsExist(S, {"operator"});
        auto optype = getReqString(S, "operator");
        if (optype.compare("VSXFormatter") == 0) {
          addVSXFormatterToGraph(graph, inputs[i]);
        } else if (optype.compare("Hilbert") == 0) {
          addHilbertToGraph(graph, inputs[i]);
        } else if (optype.compare("Demodulate") == 0) {
          addDemodulateToGraph(graph, inputs[i]);
        } else if (optype.compare("Decimate") == 0) {
          addDecimateToGraph(graph, inputs[i]);
        } else if (optype.compare("Focus") == 0) {
          addFocusToGraph(graph, inputs[i]);
        } else if (optype.compare("FocusLUT") == 0) {
          addFocusLUTToGraph(graph, inputs[i]);
        } else if (optype.compare("ChannelSum") == 0) {
          addChannelSumToGraph(graph, inputs[i]);
        } else if (optype.compare("Bmode") == 0) {
          addBmodeToGraph(graph, inputs[i]);
        } else if (optype.compare("Refocus") == 0) {
          addRefocusToGraph(graph, inputs[i]);
        } else if (optype.compare("ScanConvert2D") == 0) {
          addScanConvert2DToGraph(graph, inputs[i]);
        } else if (optype.compare("SVDFilter") == 0) {
          addSVDFilterToGraph(graph, inputs[i]);
        } else {
          printErr("Operator {} is not recognized.", optype);
        }
        slog->flush();
      }

      // Copy data into input tensor and execute graph
      std::visit(
          [&](auto &&t) {
            loadTensor(t.get(), inputs[start_idx], graph.stream);
          },
          graph.tns["input"]);
      execute(graph);

      int nlhs = outputs.size();
      int ngops = graph.ops.size();
      if (nlhs > ngops) printErr("Too many output arguments.");
      for (int i = 0; i < nlhs; i++)
        std::visit(
            [&](auto &&op) { outputs[i] = copyToMATLABArray(op->getOutput()); },
            graph.ops[graph.op_names[ngops - i - 1]]);
    } catch (const CUDARuntimeError &e) {
      graph_map.clear();
      // Intercept the error and add user instructions
      auto err = CUDARuntimeError(std::string(e.what()) +
                                  "\nYou *must* call 'clear rtbf_mex' now. "
                                  "Otherwise, MATLAB may crash.");
      throw(err);
    } catch (...) {
      throw;
    }
  }

  // Graph construction
  /// @brief Push back a generic MATLAB array to a named Tensor to `tns`.
  void addTensor(GraphData &graph, mdat::Array arr, std::string name = "",
                 bool verb = false);
  /// @brief Push back a typed MATLAB array to a named Tensor to `tns`.
  template <typename T>
  void addTensor(GraphData &graph, mdat::TypedArray<T> arr, std::string name,
                 bool verb = false);
  /// @brief Push back a shared pointer to a named Tensor `tns`.
  template <typename T>
  void addTensor(GraphData &graph, std::shared_ptr<Tensor<T>> a,
                 std::string name = "", bool verb = false);
  /// @brief Add a MATLAB array InputLoader to an execution graph.
  void addInputLoaderToGraph(GraphData &graph, const mdat::Array &arr,
                             std::vector<size_t> dims = {});
  /// @brief Parse a MATLAB struct and add VSXFormatter to an execution graph.
  void addVSXFormatterToGraph(GraphData &graph, const mdat::StructArray &S);
  /// @brief Parse a MATLAB struct and add Hilbert to an execution graph.
  void addHilbertToGraph(GraphData &graph, const mdat::StructArray &S);
  /// @brief Parse a MATLAB struct and add Demodulate to an execution graph.
  void addDemodulateToGraph(GraphData &graph, const mdat::StructArray &S);
  /// @brief Parse a MATLAB struct and add Decimate to an execution graph.
  void addDecimateToGraph(GraphData &graph, const mdat::StructArray &S);
  /// @brief Parse a MATLAB struct and add FocusLUT to an execution graph.
  void addFocusLUTToGraph(GraphData &graph, const mdat::StructArray &S);
  /// @brief Parse a MATLAB struct and add Focus to an execution graph.
  void addFocusToGraph(GraphData &graph, const mdat::StructArray &S);
  /// @brief Parse a MATLAB struct and add ChannelSum to an execution graph.
  void addChannelSumToGraph(GraphData &graph, const mdat::StructArray &S);
  /// @brief Parse a MATLAB struct and add Bmode to an execution graph.
  void addBmodeToGraph(GraphData &graph, const mdat::StructArray &S);
  /// @brief Parse a MATLAB struct and add Refocus to an execution graph.
  void addRefocusToGraph(GraphData &graph, const mdat::StructArray &S);
  /// @brief Parse a MATLAB struct and add ScanConvert2D to an execution graph.
  void addScanConvert2DToGraph(GraphData &graph, const mdat::StructArray &S);
  /// @brief Parse a MATLAB struct and add SVDFilter to an execution graph.
  void addSVDFilterToGraph(GraphData &graph, const mdat::StructArray &S);
  /// @brief Get a VSXSampleMode enum from a string
  VSXSampleMode getVSXSampleMode(std::string str);

  // Real-time operations
  /// @brief Load data from a generic MATLAB array into an existing Tensor.
  template <typename Tc>
  void loadTensor(Tensor<Tc> *t, mdat::Array a, const cudaStream_t &s);
  /// @brief Load data from a typed MATLAB array into an existing Tensor.
  template <typename Tm, typename Tc>
  void loadTensor(Tensor<Tc> *t, mdat::TypedArray<Tm> m, const cudaStream_t &s);
  /// @brief Execute all registered functions in `fns`.
  void execute(GraphData &graph);

  /// @brief Add any Operator and function call to a given execution graph.
  /// Adding arbitrary Operators and execution functions to the execution
  /// graph may sound straightforward and the code may seem short and simple,
  /// but this function is extremely complicated and required many sleepless
  /// nights of hacking away on godbolt.org to get the magical syntax just
  /// right. There are many things happening within these few lines of code. I
  /// will attempt to highlight a few key points:
  ///
  /// 1. The first template parameter is itself a template class (Op), which
  ///    is used to create the templated Operator. The instantiated Operator
  ///    is wrapped in a shared pointer that is eventually moved into
  ///    ops[graph] to extend its lifetime beyond the local scope of this
  ///    function. NOTE: This would preferably be a unique pointer, but a bug
  ///    in cuda::std::complex led to the shared pointer workaround.
  /// 2. The Operator is instantiated using a shared pointer to the input
  ///    Tensor, as well as using the arbitrary number of arguments necessary
  ///    to initialize whichever type of Operator is being requested. This is
  ///    accomplished using template parameter packs with the ... syntax to
  ///    allow addOperator to be used to initialize all Operators, regardless
  ///    of argument types or count.
  /// 3. The shared pointer to the instantiated Operator's output Tensor is also
  ///    added to the Tensor list to extend its lifetime beyond the local
  ///    scope.
  /// 4. The Operator's executable function is provided as a function pointer,
  ///    and stored in fns[graph] to be called later. To make this work with
  ///    the Operator's template parameters T_in and T_out, the function
  ///    should be wrapped in a lambda function with a constexpr to ensure
  ///    that it is only applied to the correct Operator<> type. This function
  ///    will be wrapped in a std::visit over the OperatorType variant (and
  ///    yet another lambda).
  /// 5. Care is taken to ensure that the stored lambda function pointer
  ///    passes the Operator's location by value and not by reference. For
  ///    example, if ops[graph] was a std::vector and we used back() instead
  ///    of at(idx), execution would use the last Operator in the graph for
  ///    all operations. For the std::map implementation of ops[graph], we
  ///    need to explicitly bake in the key of interest via passing by value.
  ///    In particular, we capture ops by reference and func, graph, and idx
  ///    by value to ensure that their current values are preserved.
  template <template <typename, typename> class Op, typename T_out = cfloat,
            typename T_in, typename Fn, class... Args>
  void addOperator(GraphData &graph, std::string name, std::string output_name,
                   Fn func, std::shared_ptr<Tensor<T_in>> in, Args... args) {
    auto op = std::make_shared<Op<T_in, T_out>>(in, args...);
    addTensor(graph, op->getOutput(), output_name, graph.verbose);
    graph.lastTensor = output_name;
    op->setLabel(name);
    graph.ops[name] = std::move(op);
    graph.op_names.push_back(name);
    graph.fns.push_back([&, func, name] {
      std::visit([func, name](auto &&x) { (func)(x); }, graph.ops[name]);
    });
  }
};

#endif
