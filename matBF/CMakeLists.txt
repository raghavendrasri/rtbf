project(matBF LANGUAGES CUDA CXX)

# There seems to be something wrong with CMake 3.22 in how it detects MATLAB
if(${CMAKE_MAJOR_VERSION} EQUAL 3 AND ${CMAKE_MINOR_VERSION} EQUAL 22)
  message(FATAL_ERROR "CMake version 3.22 has a bug in how it detects MATLAB. Try a different version.")
endif()

# Tested on R2019b and later.
find_package(Matlab 9.7 REQUIRED)

# Define a function to output all MATLAB MEX files to the same directories as their source files
function(RTBF_SET_MEX_OUTPUT_DIRECTORY TGT CWD)
  if(UNIX)
    set(OUTPUTCONFIGLIST "LIBRARY_OUTPUT_DIRECTORY") # Linux considers MEX a library
  elseif(WIN32)
    set(OUTPUTCONFIGLIST "RUNTIME_OUTPUT_DIRECTORY") # Windows considers MEX an executable
  endif()

  # Get a list of all of the cmake build configurations ("Debug", "Release", etc.)
  foreach(CONFIGTYPE ${CMAKE_CONFIGURATION_TYPES})
    string(TOUPPER ${CONFIGTYPE} CONFIGTYPE)

    if(UNIX)
      list(APPEND OUTPUTCONFIGLIST "LIBRARY_OUTPUT_DIRECTORY_${CONFIGTYPE}")
    elseif(WIN32)
      list(APPEND OUTPUTCONFIGLIST "RUNTIME_OUTPUT_DIRECTORY_${CONFIGTYPE}")
    endif()
  endforeach()

  foreach(OUTPUTCONFIG ${OUTPUTCONFIGLIST})
    set_target_properties(${TGT} PROPERTIES ${OUTPUTCONFIG} ${CWD})
  endforeach()
endfunction()

set(TARGETS_TO_BUILD rtbf_mex)

# Loop over all targets
foreach(CURRENT_TARGET ${TARGETS_TO_BUILD})
  # Create MEX function
  matlab_add_mex(
    NAME ${CURRENT_TARGET}
    SRC ${CURRENT_TARGET}.cu VSXFormatter.cu
    LINK_TO gpuBF
  )

  # Need this line for proper linking with CUDA
  set_target_properties(${CURRENT_TARGET} PROPERTIES CUDA_RESOLVE_DEVICE_SYMBOLS ON)

  # Ask CMake to output the mex function into the current directory)
  RTBF_SET_MEX_OUTPUT_DIRECTORY(${CURRENT_TARGET} ${CMAKE_CURRENT_SOURCE_DIR})
endforeach()

# include_directories(utils)

# # All subdirectories of matBF/<mode>/CMakeLists.txt will be added automatically.
# # For example, matBF/bmode/CMakeLists.txt.
# file(GLOB MYSUBDIRS */CMakeLists.txt)
# foreach(SUBDIRLIST ${MYSUBDIRS})
# string(REPLACE "/CMakeLists.txt" "" SUBDIR ${SUBDIRLIST})
# message(STATUS "Adding ${SUBDIR}")
# add_subdirectory(${SUBDIR})
# endforeach()