/**
 @file gpuBF/Bmode.cuh
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2022-03-29

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef RTBF_BMODE_CUH_
#define RTBF_BMODE_CUH_

#include "Operator.cuh"

namespace rtbf {

/**	@brief Class to make B-mode images from delay-and-summed data.

This class makes B-mode images from delay-and-summed data. The class performs
the following operations:

  1. Envelope detection (element-wise absolute value)
  2. Incoherent compounding (if more than 1 channel)
  3. Compression, if requested (logarithmic, power)

*/
template <typename T_in, typename T_out>
class Bmode : public Operator<Tensor<T_in>, Tensor<T_out>> {
 private:
  int gpuID;                  ///< Execution device
  std::optional<int> axis;    ///< Dimension to compound (-1 for no compounding)
  std::vector<size_t> idims;  ///< Dimensions of input
  std::vector<size_t> odims;  ///< Dimensions of output
  void execute_core(int mode, float arg = 0.f);  ///< Core execution function

 public:
  /// @brief Constructor for Bmode with a Tensor input
  Bmode(std::shared_ptr<Tensor<T_in>> input,
        std::optional<int> compoundingAxis = std::nullopt,
        cudaStream_t cudaStream = 0, std::string moniker = "Bmode",
        std::string loggerName = "");
  /// @brief Destructor for Bmode
  virtual ~Bmode();

  /// @brief Detect magnitude of complex data
  void getEnvelope();
  /// @brief Detect magnitude of complex data and apply logarithmic compression
  void getEnvelopeLogCompress();
  /// @brief Detect magnitude of complex data and apply power compression
  void getEnvelopePowCompress(float gamma = 0.3);
};

namespace kernels::Bmode {

/// @brief Include a CPU implementation of envelope detection just for fun
template <typename T_in, typename T_out>
void h_envDetect(T_in *idata, std::vector<size_t> idims, T_out *odata,
                 std::vector<size_t> odims);
/// @brief Include a CPU implementation of logarithmic compression just for fun
template <typename T_out>
void h_logCompress(T_out *odata, std::vector<size_t> odims);
/// @brief Include a CPU implementation of power compression just for fun
template <typename T_out>
void h_powCompress(T_out *odata, std::vector<size_t> odims, float gamma);

/// @brief CUDA kernel for envelope detection
template <typename T_in, typename T_out>
__global__ void envDetect(T_in *idata, dim3 idims, size_t ipitch, T_out *odata,
                          dim3 odims, size_t opitch);

/// @brief CUDA kernel for envelope detection and logarithmic compression
template <typename T_in, typename T_out>
__global__ void envDetectLogComp(T_in *idata, dim3 idims, size_t ipitch,
                                 T_out *odata, dim3 odims, size_t opitch);
/// @brief CUDA kernel for envelope detection and power compression
template <typename T_in, typename T_out>
__global__ void envDetectPowComp(T_in *idata, dim3 idims, size_t ipitch,
                                 T_out *odata, dim3 odims, size_t opitch,
                                 float gamma);
}  // namespace kernels::Bmode

template <typename>
struct is_Bmode : std::false_type {};
template <typename T_in, typename T_out>
struct is_Bmode<Bmode<T_in, T_out>> : std::true_type {};
}  // namespace rtbf

#endif  // RTBF_BMODE_CUH_
