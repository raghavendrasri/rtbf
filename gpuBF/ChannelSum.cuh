/**
 @file gpuBF/ChannelSum.cuh
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2022-03-29

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef CHANNELSUM_CUH_
#define CHANNELSUM_CUH_

#include "Decimate.cuh"
#include "Operator.cuh"

namespace rtbf {

/**	@brief Class to sum channel signals.

This class sums data across an arbitrary dimension, including partial sums into
subarrays.

ChannelSum is a light wrapper over the Decimate class, since they perform
identical operations.

*/

template <typename T_in, typename T_out>
class ChannelSum : public Operator<Tensor<T_in>, Tensor<T_out>> {
 private:
  int axis;        ///< Axis to sum
  int nchans_out;  ///< Number of channels to output (1 by default, > 1 for
                   /// subaperture beamforming)
  std::shared_ptr<Decimate<T_in, T_out>> D;  ///< Channel summing is decimation.

 public:
  ChannelSum(std::shared_ptr<Tensor<T_in>> input, int axisToSum = -1,
             int nOutputChannels = 1, cudaStream_t cudaStream = 0,
             std ::string moniker = "ChannelSum", std::string loggerName = "");
  virtual ~ChannelSum();

  /// @brief Sum channels together
  void sumChannels(bool normalize = false);
};

template <typename>
struct is_ChannelSum : std::false_type {};
template <typename T_in, typename T_out>
struct is_ChannelSum<ChannelSum<T_in, T_out>> : std::true_type {};
}  // namespace rtbf

#endif /* CHANNELSUM_CUH_ */
