/**
 @file gpuBF/Refocus.cuh
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2022-05-05

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef REFOCUS_CUH_
#define REFOCUS_CUH_

#include <cublas_v2.h>

#include "Operator.cuh"

namespace rtbf {

/** @brief Class to apply REFoCUS to data.
 */
template <typename T_in, typename T_out>
class Refocus : public Operator<Tensor<T_in>, Tensor<T_out>> {
 protected:
  // CUDA objects
  int gpuID;
  cublasHandle_t handle;  ///< CUBLAS handle
  cufftHandle fftplan1;   ///< CUFFT plan object
  cufftHandle fftplan2;   ///< CUFFT plan object
  float fs;               ///< Sampling frequency
  size_t nsamps;          ///< Number of samples in the original image
  size_t nxmits;          ///< Number of transmit events
  size_t nelemT;          ///< Number of transmit elements
  size_t nelemR;          ///< Number of receive elements
  size_t nframe;          ///< Number of frames
  size_t frameStride;     ///< Number of samples between subsequent frames
  Tensor<float> txd;      ///< Transmit delays [nxmits, nelemT]
  Tensor<float> txa;      ///< Transmit apodization [nxmits, nelemT]
  Tensor<cuda::std::complex<float>> E;    ///< Transmitter delays encoder
  Tensor<cuda::std::complex<float>> D;    ///< Transmitter delays decoder
  Tensor<cuda::std::complex<float>> tmp;  ///< Dummy array

  /// @brief Utility function to load inputs the same way across all ctors.
  void loadInputs(std::shared_ptr<Tensor<T_in>> input, cudaStream_t cudaStream,
                  std ::string moniker, std::string loggerName);
  /// @brief Utility function to initialize cuFFT for all constructors.
  void initHandles();
  /// @brief Utility function to initialize tmp and output for all ctors.
  void initOutput();

  /// @brief Construct the encoder from the transmit delay and apodizations.
  void makeEncoder();

  /// @brief Construct the decoder need to retrieve multistatic data.
  void makeDecoder();

  // Internal functions that are invoked by makeDecoder
  /// @brief Part 1 of Tikhonov-regularized inversion
  void tikhonov_EhE(Tensor<cuda::std::complex<float>> &E);
  /// @brief Part 2 of Tikhonov-regularized inversion
  void tikhonov_invEhE(Tensor<cuda::std::complex<float>> &invEhE,
                       Tensor<cuda::std::complex<float>> &EhE);
  /// @brief Part 3 of Tikhonov-regularized inversion
  void tikhonov_EinvEhE(Tensor<cuda::std::complex<float>> &invEhE);

 public:
  /// @brief Simple constructor with transmit delays and apodizations
  Refocus(std::shared_ptr<Tensor<T_in>> input, const Tensor<float> *h_txDelays,
          const Tensor<float> *h_txApods, float samplingFrequency,
          cudaStream_t cudaStream = 0, std ::string moniker = "Refocus",
          std::string loggerName = "");
  /// @brief Alternate constructor specifying encoder or decoder explicitly
  Refocus(std::shared_ptr<Tensor<T_in>> input,
          const Tensor<cuda::std::complex<float>> *h_encoder = nullptr,
          const Tensor<cuda::std::complex<float>> *h_decoder = nullptr,
          cudaStream_t cudaStream = 0, std ::string moniker = "Refocus",
          std::string loggerName = "");
  virtual ~Refocus();

  /// @brief Decode the encoded transmit to recover the multistatic data.
  void decode();
};

namespace kernels::Refocus {
/// @cond KERNELS
/**	@addtogroup Refocus Kernels
        @{
*/
/** @brief Naive kernel for out-of-place matrix transpose. Can optimize if
 necessary.
 @param src Pointer to source matrix
 @param dst Pointer to destination matrix
 @param nx_dst Inner dimension size for destination matrix
 @param ny_dst Outer dimension size for destination matrix
 @param alpha Multiplicative factor (scalar)
*/
template <typename T_in, typename T_out>
__global__ void transpose(T_in *A, T_out *B, int nx, int ny);

__global__ void makeEncoder(cuda::std::complex<float> *enc, float *txdel,
                            float *txapo, float fs, int nelemT, int nxmits,
                            int nsamps);
__global__ void loadDiagonal(cuda::std::complex<float> *ete, float lambda,
                             int nxmits, int nsamps);

/** @}*/
/// @endcond

}  // namespace kernels::Refocus

// Add template type traits for SFINAE
template <typename>
struct is_Refocus : std::false_type {};
template <typename T_in, typename T_out>
struct is_Refocus<Refocus<T_in, T_out>> : std::true_type {};

}  // namespace rtbf

#endif /* REFOCUS_CUH_ */
