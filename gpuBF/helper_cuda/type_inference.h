/**
 @file gpuBF/helper_cuda/rtbf_type_inference.cu
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2022-04-21

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef RTBF_TYPE_INFERENCE_CUH_
#define RTBF_TYPE_INFERENCE_CUH_
#include <complex>
#include <cuda/std/complex>

#include "Bmode.cuh"
#include "ChannelSum.cuh"
#include "Focus.cuh"
#include "FocusLUT.cuh"
#include "Hilbert.cuh"
#include "gpuBF.cuh"

template <typename T>
std::string type_name() {
  return "unknown";
}
template <>
std::string type_name<double>() {
  return "double";
}
template <>
std::string type_name<float>() {
  return "float";
}
template <>
std::string type_name<int>() {
  return "int";
}
template <>
std::string type_name<short>() {
  return "short";
}
template <>
std::string type_name<std::complex<double>>() {
  return "complex<double>";
}
template <>
std::string type_name<std::complex<float>>() {
  return "complex<float>";
}
template <>
std::string type_name<std::complex<int>>() {
  return "complex<int>";
}
template <>
std::string type_name<std::complex<short>>() {
  return "complex<short>";
}
template <>
std::string type_name<cuda::std::complex<double>>() {
  return "complex<double>";
}
template <>
std::string type_name<cuda::std::complex<float>>() {
  return "complex<float>";
}
template <>
std::string type_name<cuda::std::complex<int>>() {
  return "complex<int>";
}
template <>
std::string type_name<cuda::std::complex<short>>() {
  return "complex<short>";
}

#endif