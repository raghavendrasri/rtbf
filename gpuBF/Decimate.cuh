/**
 @file gpuBF/Decimate.cuh
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2022-08-08

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef DECIMATE_CUH_
#define DECIMATE_CUH_

#include "Operator.cuh"

namespace rtbf {

/**	@brief Class to sum channel signals.

This class sums data across an arbitrary dimension, including partial sums into
subarrays.

*/
template <typename T_in, typename T_out>
class Decimate : public Operator<Tensor<T_in>, Tensor<T_out>> {
 private:
  int gpuID;  ///< Device to execute on
  int axis;   ///< Axis to sum
  int dsf;    ///< Downsample factor
  int nout;   ///< Number of samples in the decimated signal
  std::vector<size_t> idims, odims;
  bool isZeroCopy = false;  ///< Is this an identity operation

  /// @brief Special case of decimation in the first dimension
  void decimateDim0(bool normalize);

 public:
  Decimate(std::shared_ptr<Tensor<T_in>> input, int downsampleFactor,
           int axisToDecimate = 0, cudaStream_t cudaStream = 0,
           std ::string moniker = "Decimate", std::string loggerName = "");
  virtual ~Decimate();

  /// @brief Sum channels together
  void decimate(bool normalize = false);
};

namespace DecimateKernels {

template <typename T_in, typename T_out>
void h_decimate(T_in *idata, std::vector<size_t> idims, size_t ipitch,
                T_out *odata, std::vector<size_t> odims, size_t opitch, int ds,
                bool normalize);

template <bool normalize, typename T_in, typename T_out>
__global__ void decimate(T_in *idata, dim3 idims, int ipitch, T_out *odata,
                         dim3 odims, int opitch, int ds, int nframes);
template <typename T_in, typename T_out>
void h_decimateDim0(T_in *idata, std::vector<size_t> idims, size_t ipitch,
                    T_out *odata, std::vector<size_t> odims, size_t opitch,
                    int ds, bool normalize);

template <bool normalize, typename T_in, typename T_out>
__global__ void decimateDim0(T_in *idata, dim3 idims, int ipitch, T_out *odata,
                             dim3 odims, int opitch, int ds);
}  // namespace DecimateKernels

template <typename>
struct is_Decimate : std::false_type {};
template <typename T_in, typename T_out>
struct is_Decimate<Decimate<T_in, T_out>> : std::true_type {};
}  // namespace rtbf

#endif /* DECIMATE_CUH_ */
