/**
 @file gpuBF/gpuBF.cuh
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2019-03-04

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef GPUBF_CUH_
#define GPUBF_CUH_

#include <cuda.h>
#include <cuda_fp16.h>
#include <cufft.h>
#include <helper_cuda.h>
#include <helper_math.h>
#include <vector_types.h>

#include <complex>
#include <cuda/std/complex>
#include <fstream>
#include <functional>
#include <iostream>
#include <optional>
#include <string>
#include <type_traits>
#include <typeinfo>
#include <vector>

#include "spdlog/fmt/ostr.h"
#include "spdlog/sinks/rotating_file_sink.h"  // support for basic file logging
#include "spdlog/sinks/stdout_color_sinks.h"
#include "spdlog/spdlog.h"

// Macro for excessively long names
#define CCE checkCudaErrors

// Compiler-independent function that suppresses spurious "unreachable" warnings
#ifdef __GNUC__  // GCC 4.8+, Clang, Intel and other compilers compatible with
                 // GCC (-std=c++0x or above)
[[noreturn]] inline __attribute__((always_inline)) __host__ __device__ void
unreachable() {
  __builtin_unreachable();
}
#elif defined(_MSC_VER)  // MSVC
[[noreturn]] __forceinline __host__ __device__ void unreachable() {
  __assume(false);
}
#else                    // ???
inline void unreachable() {}
#endif

namespace rtbf {

enum class BFMode { IDENTITY, SUM, BLKDIAG };

/// @brief Utility to determine if type is complex at compile-time
template <typename T>
constexpr __device__ __host__ bool is_complex() {
  // Check for both std::complex and cuda::std::complex types
  return std::is_same_v<T, std::complex<decltype(abs(T(0)))>> ||
         std::is_same_v<T, cuda::std::complex<decltype(abs(T(0)))>>;
}

/// @brief Utility to determine if type is complex at compile-time
template <typename T>
constexpr __device__ __host__ bool is_cuda_complex() {
  // Check for both std::complex and cuda::std::complex types
  return std::is_same_v<T, cuda::std::complex<decltype(abs(T(0)))>>;
}

/// @brief Utility to determine if type is complex at compile-time
template <typename T>
constexpr __device__ __host__ bool is_complex_float() {
  // Check for both std::complex and cuda::std::complex types
  return is_complex<T>() && std::is_floating_point_v<decltype(abs(T(0)))>;
}

// Use template metaprogramming to determine what types to read from the CUDA
// texture objects. For ordinary arithmetic scalar types, just load the type
// itself. For cuda::std::complex<T> types, load the 2-vector datatype instead
// (e.g., float2, int2, short2).
template <typename T>
struct TexType {
  using type = T;
};
template <>
struct TexType<cuda::std::complex<float>> {
  using type = float2;
};
template <>
struct TexType<cuda::std::complex<short>> {
  using type = short2;
};
template <>
struct TexType<cuda::std::complex<int>> {
  using type = int2;
};

/// @brief Explicit cast to a cuda::std::complex datatype
/// Uses template metaprogramming.
template <typename T_out, typename T_in>
inline __host__ __device__ auto complex_cast(const T_in &x) {
  if constexpr (is_cuda_complex<T_out>()) {
    if constexpr (is_cuda_complex<T_in>()) {
      return T_out(x.real(), x.imag());
    } else {
      return T_out(x, 0);
    }
  } else {
    if constexpr (is_cuda_complex<T_in>()) {
      return cuda::std::complex<T_out>(x.real(), x.imag());
    } else {
      return cuda::std::complex<T_out>(x, 0);
    }
  }
  // Compiler throws a spurious (incorrect) warning about a missing return
  // statement as of CUDA 11.4. Suppress the warning.
  unreachable();
}

/// @brief Simple absf function to compute abs on arbitrary complex datatypes
template <typename T>
inline __host__ __device__ float absf(cuda::std::complex<T> d) {
  return abs(cuda::std::complex<float>(real(d), imag(d)));
}

/// @brief Simple absf function to compute abs on arbitrary complex datatypes
template <typename T>
inline __host__ float absf(std::complex<T> d) {
  return abs(std::complex<float>(real(d), imag(d)));
}

/// @brief Simple function to cast a <T>2 to a complex<T>
inline __host__ __device__ cuda::std::complex<short> castToComplex(short2 a) {
  return cuda::std::complex<short>(a.x, a.y);
}
inline __host__ __device__ cuda::std::complex<int> castToComplex(int2 a) {
  return cuda::std::complex<int>(a.x, a.y);
}
inline __host__ __device__ cuda::std::complex<float> castToComplex(float2 a) {
  return cuda::std::complex<float>(a.x, a.y);
}
inline __host__ __device__ short2 castFromComplex(cuda::std::complex<short> a) {
  return make_short2(a.real(), a.imag());
}
inline __host__ __device__ int2 castFromComplex(cuda::std::complex<int> a) {
  return make_int2(a.real(), a.imag());
}
inline __host__ __device__ float2 castFromComplex(cuda::std::complex<float> a) {
  return make_float2(a.real(), a.imag());
}

/// @brief Utility to compute e^{2j*pi*phi} and return a complex float
inline __host__ __device__ cuda::std::complex<float> expj2pi(float phi) {
  float2 phasor;
  sincospif(2.f * phi, &phasor.y, &phasor.x);
  return castToComplex(phasor);
}

/// @brief Compute the product of a vector of arbitrary type T
template <typename T>
T prod(std::vector<T> data) {
  T p = 1;
  for (auto &d : data) p *= d;
  return p;
}

/// @brief Print a nicely formatted string of vector data as (a, b, c, ...)
template <typename T>
std::string vecString(std::vector<T> data) {
  std::string s = "(";
  if (!data.empty()) {
    s += fmt::format("{}", data[0]);
    for (auto it = std::next(data.begin()); it != data.end(); ++it)
      s += fmt::format(", {}", *it);
  }
  s += ")";
  return s;
}

template <typename T>
T vecNorm(std::vector<T> a, std::vector<T> b) {
  int n = a.size();
  T sq = T(0);
  for (int i = 0; i < n; i++) sq += (a[i] - b[i]) * (a[i] - b[i]);
  return sqrt(sq);
}

/// @brief Return a linear index for an n-dimensional array
template <typename T, typename Td, typename Tc>
T *getDataPtrCoords(T *data, std::vector<Td> dims, std::vector<Tc> coords,
                    size_t pitch = 0) {
  if (coords.empty() || dims.empty()) return nullptr;
  while (coords.size() < dims.size()) coords.push_back(0);
  for (int i = 0; i < dims.size(); i++) {
    if (coords[i] < 0 || coords[i] >= dims[i]) return nullptr;
  }
  // The linear index of n-dimensional coordinates (a, b, ..., y, z) is found
  // as idx = a + na * (b + nb * (... (y + ny*z) ...)) ptrdiff_t's are used
  // just in case int is not large enough to index the data
  ptrdiff_t idx = coords.back();
  for (auto i = coords.size() - 1; i >= 1; i--) idx = idx * dims[i] + coords[i];
  if (pitch == 0) pitch = dims[0];
  idx = idx * pitch + coords[0];  // Use pitch instead of dims[0]
  ptrdiff_t imin = 0;
  ptrdiff_t imax = prod(dims) * pitch / dims[0];
  if (idx < imin || idx >= imax) return nullptr;  // Probably redundant
  return &data[idx];
}

/// @brief Convert n-D size_t vector -> 3D int vector for CUDA kernel launches
// If dims are {a, b, c, ..., y, z}, the output is {a, b*c*...*y, z}.
template <typename T>
dim3 dimsForCUDAKernels(std::vector<T> dims) {
  dim3 d(1, 1, 1);
  T n = dims.size();
  if (n == 1) {
    d.x = dims.front();
  } else if (n == 2) {
    d.x = dims.front();
    d.y = dims.back();
  } else {
    d.x = dims.front();
    d.y = prod(std::vector<T>(std::next(dims.begin()), std::prev(dims.end())));
    d.z = dims.back();
  }
  return d;
}

/// @brief Add a "not implemented" exception.
class NotImplemented : public std::logic_error {
 public:
  NotImplemented(std::string str)
      : std::logic_error(str + " Function not yet implemented"){};
};

}  // namespace rtbf

#endif /* GPUBF_CUH_ */
