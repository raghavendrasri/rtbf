/**
 @file gpuBF/Hilbert.cu
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2022-04-21

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "Hilbert.cuh"

namespace rtbf {

template <typename T_in, typename T_out>
Hilbert<T_in, T_out>::Hilbert(std::shared_ptr<Tensor<T_in>> input,
                              cudaStream_t cudaStream, std::string moniker,
                              std::string loggerName) {
  this->label = moniker;
  this->setLogger(loggerName);

  // Parse inputs
  this->in = input;                  // Share ownership of input Tensor
  this->stream = cudaStream;         // Set asynchronous stream
  gpuID = this->in->getDeviceID();   // Execute on input's device
  dims = this->in->getDimensions();  // Get the input data dimensions
  if (dims.empty()) this->logwarn("Hilbert transform of an empty Tensor.");
  if (gpuID < 0)
    this->template logerror<NotImplemented>("Cannot execute on CPU Tensors.");

  // Create input array
  if constexpr (is_complex_float<T_in>()) {
    // If the input has complex<float> type, reuse.
    tmp = this->in;
  } else {
    // Otherwise, create a temporary FFT Tensor
    tmp = std::make_shared<Tensor<cuda::std::complex<float>>>(
        this->in->getDimensions(), gpuID, this->label + "->tmp", this->logName);
  }

  // Create output array
  if constexpr (is_complex_float<T_out>()) {
    // If the output also has complex<float> type, reuse.
    this->out = tmp;
  } else {
    // Otherwise, create output Tensor
    this->out = std::make_shared<Tensor<T_out>>(
        this->in->getDimensions(), gpuID, this->label + "->out", this->logName);
  }

  // Set up CUFFT plans
  if (!dims.empty()) {
    int x = dims[0];
    int p = tmp->getPitch();
    int n = prod(dims) / x;
    CCE(cudaSetDevice(gpuID));
    CCE(cufftCreate(&fftplan));
    CCE(cufftPlanMany(&fftplan, 1, &x, &p, 1, p, &p, 1, p, CUFFT_C2C, n));
    CCE(cufftSetStream(fftplan, this->stream));
  }
}

template <typename T_in, typename T_out>
Hilbert<T_in, T_out>::~Hilbert() {
  if (fftplan != 0) cufftDestroy(fftplan);
}

// Hilbert transform of in RF data when using a temporary array
template <typename T_in, typename T_out>
void Hilbert<T_in, T_out>::transform() {
  if (dims.empty()) return;  // Do nothing if empty Tensors.
  tmp->resetToZerosAsync(this->stream);
  this->logdebug("Applying Hilbert transform.");

  // Get pointers
  T_in *iptr = this->in->data();
  T_out *optr = this->out->data();
  auto *tptr = tmp->data();
  int nsamps = dims[0];
  int nlines = prod(dims) / nsamps;
  int ip = this->in->getPitch();
  int tp = tmp->getPitch();
  int op = this->out->getPitch();
  dim3 B(16, 16, 1);
  dim3 G((nsamps - 1) / B.x + 1, (nlines - 1) / B.y + 1, 1);

  // If a cast-and-copy is needed for [in->tmp]
  if constexpr (!is_complex_float<T_in>()) {
    kernels::Hilbert::castToComplexFloat<<<G, B, 0, this->stream>>>(
        tptr, tp, iptr, ip, nsamps, nlines);
  }

  auto *cptr = reinterpret_cast<cufftComplex *>(tmp->data());
  CCE(cufftExecC2C(fftplan, cptr, cptr, CUFFT_FORWARD));
  kernels::Hilbert::bpfilter<<<G, B, 0, this->stream>>>(tptr, tp, nsamps,
                                                        nlines);
  CCE(cufftExecC2C(fftplan, cptr, cptr, CUFFT_INVERSE));

  // If a cast-and-copy is needed for [tmp->out]
  if constexpr (!is_complex_float<T_out>()) {
    kernels::Hilbert::castFromComplexFloat<<<G, B, 0, this->stream>>>(
        optr, op, tptr, tp, nsamps, nlines);
  }
}

/** @brief Bandpass filtering for anti-aliasing the upsampled data.
Verasonics data is already demodulated. The upsampling procedure in
unwrapIQ results in an aliased spectrum. This kernel applies an
anti-aliasing bandpass filter whose band depends on the samples per cycle of the
VSXSampleMode.
*/
__global__ void kernels::Hilbert::bpfilter(cuda::std::complex<float> *data,
                                           int pitch, int nsamps, int nlines) {
  // Determine information about the current thread
  int samp = blockIdx.x * blockDim.x + threadIdx.x;
  int line = blockIdx.y * blockDim.y + threadIdx.y;

  // Only execute kernel if valid
  if (samp < nsamps && line < nlines) {
    // Each frequency will be weighted according to the sampling mode
    float weight;
    // In Hilbert transform mode, we zero out the negative part of the frequency
    // spectrum (i.e. when normalized frequency > 0.5).
    if (nsamps & 1) {  // Hilbert Transform (odd nsamps)
      if (samp == 0) {
        weight = 1.f / nsamps;
      } else if (samp < (nsamps + 1) / 2) {
        weight = 2.f / nsamps;
      } else {
        weight = 0.f / nsamps;
      }
    } else {  // Hilbert Transform (even nsamps)
      if (samp == 0 || samp == nsamps / 2) {
        weight = 1.f / nsamps;
      } else if (samp < nsamps / 2) {
        weight = 2.f / nsamps;
      } else {
        weight = 0.f / nsamps;
      }
    }
    // Apply weights to current sample
    data[samp + pitch * line] *= weight;
  }
}

template <typename T>
__global__ void kernels::Hilbert::castToComplexFloat(
    cuda::std::complex<float> *dst, int dpitch, T *src, int spitch, int nsamps,
    int nlines) {
  int samp = blockIdx.x * blockDim.x + threadIdx.x;
  int line = blockIdx.y * blockDim.y + threadIdx.y;
  if (samp < nsamps && line < nlines) {
    int sidx = samp + spitch * line;
    int didx = samp + dpitch * line;
    dst[didx] = complex_cast<float>(src[sidx]);
  }
}
template <typename T>
__global__ void kernels::Hilbert::castFromComplexFloat(
    T *dst, int dpitch, cuda::std::complex<float> *src, int spitch, int nsamps,
    int nlines) {
  int samp = blockIdx.x * blockDim.x + threadIdx.x;
  int line = blockIdx.y * blockDim.y + threadIdx.y;
  if (samp < nsamps && line < nlines) {
    int sidx = samp + spitch * line;
    int didx = samp + dpitch * line;
    dst[didx] = complex_cast<T>(src[sidx]);
  }
}

// Real input
template class Hilbert<short, cuda::std::complex<short>>;
template class Hilbert<short, cuda::std::complex<float>>;
template class Hilbert<float, cuda::std::complex<short>>;
template class Hilbert<float, cuda::std::complex<float>>;
template class Hilbert<cuda::std::complex<short>, cuda::std::complex<short>>;
template class Hilbert<cuda::std::complex<short>, cuda::std::complex<float>>;
template class Hilbert<cuda::std::complex<float>, cuda::std::complex<short>>;
template class Hilbert<cuda::std::complex<float>, cuda::std::complex<float>>;

}  // namespace rtbf
