/**
 @file gpuBF/Focus.cu
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2022-04-05

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "Focus.cuh"

namespace rtbf {
using IM = InterpMode;
// using SM = SynthesisMode;
template <typename T_in, typename T_out>
Focus<T_in, T_out>::Focus(
    std::shared_ptr<Tensor<T_in>> input, const Tensor<float> *pixelPos,
    const Tensor<float> *elemPos, const Tensor<float> *txData,
    const Tensor<float> *timeZeros, float fsample, float fcenter,
    bool isInputBB, bool makeOutputBB, float txfnumber, float rxfnumber,
    float soundspeed, BFMode txbfMode, BFMode rxbfMode, InterpMode intrpMode,
    cudaStream_t cudaStream, std::string moniker, std::string loggerName) {
  this->label = moniker;
  this->setLogger(loggerName);

  if constexpr (!is_complex<T_in>()) {
    this->loginfo("Using RF (real data) mode.");
    this->logdebug(
        "Ignoring isInputBB and makeOutputBB flags because baseband data "
        "cannot be represented using real values.");
  } else {
    this->loginfo("Using IQ (complex data) mode.");
    this->logdebug(
        "Input data has fc={}Hz and fs={}Hz; Output data will have fc={}Hz.",
        isInputBB ? 0.f : fcenter, fsample, makeOutputBB ? 0.f : fcenter);
    if (isInputBB)
      this->logdebug("Original modulation frequency is {}Hz.", fcenter);
  }

  // Load input data
  this->in = input;  // Share ownership of input Tensor
  gpuID = this->in->getDeviceID();
  auto idims = this->in->getDimensions();
  nxmits = idims[1];
  nelems = idims[2];
  npixels = pixelPos->getNumberOfValidElements() / 3;
  isBBIn = isInputBB;
  isBBOut = makeOutputBB;
  if (gpuID < 0)
    this->template logerror<NotImplemented>("Cannot execute on CPU Tensors.");

  // Load scalars
  fs = fsample;
  fc = fcenter;
  txfn = txfnumber;
  rxfn = rxfnumber;
  c0 = soundspeed;
  tmode = txbfMode;
  rmode = rxbfMode;
  imode = intrpMode;
  this->stream = cudaStream;

  // Load beamforming parameters
  pxpos = Tensor<float>(*pixelPos, gpuID, "d_pxpos", this->logName);
  elpos = Tensor<float>(*elemPos, gpuID, "d_elpos", this->logName);
  txdat = Tensor<float>(*txData, gpuID, "d_txdat", this->logName);
  time0 = Tensor<float>(*timeZeros, gpuID, "d_time0", this->logName);

  // Important! Focus uses texture objects, which have special alignment
  // requirements. If input is already pitched, use as is. Otherwise, make a
  // pitched copy of the data.
  if (this->in->isPitched()) {
    in_pitched = this->in;
  } else {
    // Call copy explicit constructor
    in_pitched = std::make_shared<Tensor<T_in>>(
        *(this->in), std::nullopt, std::nullopt, std::nullopt, true);
  }

  makeOutputTensor();   // Initialize arrays for output
  initTextureObject();  // Initialize texture object for input
}
template <typename T_in, typename T_out>
Focus<T_in, T_out>::~Focus() {
  if (gpuID >= 0) {
    CCE(cudaSetDevice(gpuID));
    for (auto &t : tex)
      if (t) CCE(cudaDestroyTextureObject(t));
  }
  this->loginfo("Destroyed {}.", this->label);
}

template <typename T_in, typename T_out>
void Focus<T_in, T_out>::makeOutputTensor() {
  // Define the output dimensions according to the beamforming configuration.
  // Use the pixel positions as a guide for the eventual beamformed shape.
  // If pxpos->getDimensions() == {3, M, N, P, ...}, the output should have
  // dimensions == {M, N, P, ..., (1 or nxmits), (1 or nelems)}, depending on
  // whether the transmit and/or receive dimensions are summed.
  std::vector<size_t> odims = pxpos.getDimensions();
  odims.erase(odims.begin());  // Remove first element (xyz dimension of 3)
  while (odims.size() < 2) odims.push_back(1);

  if (tmode == BFMode::IDENTITY) {
    this->loginfo("Preserving {} individual transmits (delay, no sum).",
                  nxmits);
    odims.push_back(nxmits);  // TX dimension has nxmits
  } else if (tmode == BFMode::SUM) {
    this->loginfo("Beamforming across {} transmits (delay and sum).", nxmits);
    odims.push_back(1);  // TX dimension is 1
  } else if (tmode == BFMode::BLKDIAG) {
    this->loginfo(
        "Transmits map to disjoint sets of pixels (ordinary beamforming).");
    if (odims.back() != nxmits)
      this->template logerror<std::invalid_argument>(
          "Last pixel dimension {} does not match the number of transmits "
          "{}. Cannot use BFMode::BLKDIAG.",
          odims.back(), nxmits);
  }
  if (rmode == BFMode::IDENTITY) {
    this->loginfo("Preserving {} receive elements (delay, no sum).", nelems);
    odims.push_back(nelems);
  } else if (rmode == BFMode::SUM) {
    this->loginfo("Beamforming across {} receive elements (delay and sum).",
                  nelems);
    odims.push_back(1);
  } else if (rmode == BFMode::BLKDIAG) {
    // TODO: Possible use-case: virtual receive array
    this->template logerror<NotImplemented>(
        "Not implemented. It is currently assumed that all receive elements "
        "are focused at all pixels.");
  }
  // Include frame dimension in output
  auto idims = this->in->getDimensions();
  for (int i = 3; i < idims.size(); i++) {
    odims.push_back(idims[i]);
  }
  this->out = std::make_shared<Tensor<T_out>>(
      odims, gpuID, this->label + "->out", this->logName,
      false);  // gpuID >= 0);
}

template <typename T_in, typename T_out>
void Focus<T_in, T_out>::initTextureObject() {
  if (gpuID < 0) return;
  // Set up textures to use the built-in bilinear interpolation hardware
  // Use texture objects (CC >= 3.0)
  // Texture description
  memset(&texDesc, 0, sizeof(texDesc));
  texDesc.addressMode[0] = cudaAddressModeBorder;
  texDesc.addressMode[1] = cudaAddressModeBorder;
  texDesc.filterMode = cudaFilterModeLinear;
  texDesc.normalizedCoords = 0;
  // Read mode depends on input and output types, determined at compile time.
  if constexpr (std::is_same_v<T_in, T_out>) {
    texDesc.readMode = cudaReadModeElementType;
  } else {
    static_assert(std::is_same_v<T_out, float> ||
                      std::is_same_v<T_out, cuda::std::complex<float>>,
                  "T_out must equal T_in or be floating point.");
    texDesc.readMode = cudaReadModeNormalizedFloat;
  }
  // Resource description
  std::vector<size_t> d = this->in->getDimensions();
  while (d.size() < 3) d.push_back(1);
  size_t framesize = d[0] * d[1] * d[2];
  size_t ntex = prod(d) / framesize;
  tex.resize(ntex, 0);

  CCE(cudaSetDevice(gpuID));
  for (size_t n = 0; n < ntex; n++) {
    if (tex[n] != 0) CCE(cudaDestroyTextureObject(tex[n]));
    memset(&resDesc, 0, sizeof(resDesc));
    resDesc.resType = cudaResourceTypePitch2D;
    resDesc.res.pitch2D.width = d[0];
    resDesc.res.pitch2D.height = d[1] * d[2];
    resDesc.res.pitch2D.desc =
        cudaCreateChannelDesc<typename TexType<T_in>::type>();
    resDesc.res.pitch2D.devPtr = in_pitched->data() + n * framesize;
    resDesc.res.pitch2D.pitchInBytes = in_pitched->getPitchInBytes();
    this->logdebug(
        "Initializing CUDA texture object {} ({} x {}) from pitched memory "
        "at {}.",
        n, d[0], d[1] * d[2], fmt::ptr(resDesc.res.pitch2D.devPtr));
    // Create texture object
    CCE(cudaCreateTextureObject(&tex[n], &resDesc, &texDesc, NULL));
  }
}

template <typename T_in, typename T_out>
void Focus<T_in, T_out>::focus() {
  if (gpuID >= 0) {
    CCE(cudaSetDevice(gpuID));
    // Determine which CUDA kernel to call
    // Call the focus kernel. Explicitly specify the kernel template
    // parameters to achieve implicit template function instantiation. The
    // struct focusargs and function focusHelper are used purely to make the
    // following code more readable.
    if constexpr (is_complex<T_in>()) {
      this->logdebug("Executing IQ focus() on GPU{}", gpuID);
    } else {
      this->logdebug("Executing RF focus() on GPU{}", gpuID);
    }
    in_pitched->copyFromAsync(this->in.get(), this->stream);

    // Treat the element positions differently. If the input is a 3xN array,
    // assume (x,y,z) coordinates. If the element positions are provided as a
    // 4xN array, assume the 4th "row" of data denotes the angles of the element
    // face. Currently, this only handles rotations in the x-z plane.
    auto edims = elpos.getDimensions();
    if (edims.size() < 1) this->logerror("Invalid element position Tensor.");
    if (edims[0] == 3) {
      if (imode == IM::NEAREST) focusHelper<IM::NEAREST, float3>();
      if (imode == IM::LINEAR) focusHelper<IM::LINEAR, float3>();
      if (imode == IM::CUBIC) focusHelper<IM::CUBIC, float3>();
      if (imode == IM::LANCZOS3) focusHelper<IM::LANCZOS3, float3>();
    } else if (edims[0] == 4) {
      if (imode == IM::NEAREST) focusHelper<IM::NEAREST, float4>();
      if (imode == IM::LINEAR) focusHelper<IM::LINEAR, float4>();
      if (imode == IM::CUBIC) focusHelper<IM::CUBIC, float4>();
      if (imode == IM::LANCZOS3) focusHelper<IM::LANCZOS3, float4>();
    } else
      this->logerror("Invalid element position Tensor. Must be 3xN or 4xN.");
  }
}

template <typename T_in, typename T_out>
template <InterpMode imode, typename XYZA_rx>
void Focus<T_in, T_out>::focusHelper() {
  // XYZA_rx allows the receive elements to be specified as xyz or xyza
  this->logdebug("Executing focus with isBBin={} and isBBout={}.", isBBIn,
                 isBBOut);
  // For the actual execution, use dim3 with the following dimensions:
  //    [pixels, nxmits_out, nelems_out].
  // This will be looped over prod(dims[3:]), i.e. all "frames".
  std::vector<size_t> pdims = pxpos.getDimensions();
  auto npdims = pdims.size() - 1;
  auto npix = npixels;
  if (tmode == BFMode::BLKDIAG) {
    npix /= pdims[npdims];
    npdims -= 1;
  }
  std::vector<size_t> odims = this->out->getDimensions();
  int nxmits_out = odims[npdims];
  int nelems_out = odims[npdims + 1];
  dim3 od3(npix, nxmits_out, nelems_out);
  // Kernel launch parameters
  dim3 B(16, 4, 4);
  dim3 G((od3.x - 1) / B.x + 1, (od3.y - 1) / B.y + 1, (od3.z - 1) / B.z + 1);

  auto *ppos = reinterpret_cast<float3 *>(pxpos.data());
  auto *rpos = reinterpret_cast<XYZA_rx *>(elpos.data());
  auto *t0 = time0.data();
  int nxmits_sum = nxmits / nxmits_out;  // Number of transmits to sum
  int nelems_sum = nelems / nelems_out;  // Number of elements to sum
  int framesize = od3.x * od3.y * od3.z;
  int nframes = prod(odims) / framesize;

  // Treat the transmit data differently. If the input is a 3xN array,
  // assume (x,y,z) coordinates. If the element positions are provided as a
  // 4xN array, assume the 4th "row" of data denotes the angles of the element
  // face. Currently, this only handles rotations in the x-z plane.
  auto tdims = txdat.getDimensions();
  if (tdims.size() < 1) this->logerror("Invalid transmit data Tensor.");
  if (tmode == BFMode::BLKDIAG) {
    if (tdims[0] == 3) {
      this->logdebug("Executing block-diagonal focusing.");
      // It is more convenient to pass (x,y,z) coordinates as float3's.
      auto *tpos = reinterpret_cast<float3 *>(txdat.data());
      // Loop through frames
      this->out->resetToZeros();
      for (int f = 0; f < nframes; f++) {
        this->logdebug("Focusing frame {} of {}.", f + 1, nframes);
        T_out *d_out = this->out->data() + f * framesize;
        kernels::Focus::focus_bd<T_out, imode><<<G, B, 0, this->stream>>>(
            tex[f], ppos, npix, tpos, nxmits_out, rpos, nelems_out, fs, fc,
            1.f / c0, nelems_sum, txfn, rxfn, t0, d_out, isBBIn, isBBOut);
        getLastCudaError("Focusing kernel failed.\n");
      }
    } else if (tdims[0] == 4) {
      this->logdebug("Executing block-diagonal focusing.");
      // It is more convenient to pass (x,y,z,a) coordinates as float4's.
      auto *tpos = reinterpret_cast<float4 *>(txdat.data());
      // Loop through frames
      this->out->resetToZeros();
      for (int f = 0; f < nframes; f++) {
        this->logdebug("Focusing frame {} of {}.", f + 1, nframes);
        T_out *d_out = this->out->data() + f * framesize;
        kernels::Focus::focus_bd<T_out, imode><<<G, B, 0, this->stream>>>(
            tex[f], ppos, npix, tpos, nxmits_out, rpos, nelems_out, fs, fc,
            1.f / c0, nelems_sum, txfn, rxfn, t0, d_out, isBBIn, isBBOut);
        getLastCudaError("Focusing kernel failed.\n");
      }
    }
  } else {
    if (tdims[0] == 2) {
      this->logdebug("Executing plane wave focusing.");
      // It is more convenient to pass (theta, phi) coordinates as float2's.
      auto *vpos = reinterpret_cast<float2 *>(txdat.data());
      // Loop through frames
      this->out->resetToZeros();
      for (int f = 0; f < nframes; f++) {
        this->logdebug("Focusing frame {} of {}.", f + 1, nframes);
        T_out *d_out = this->out->data() + f * framesize;
        kernels::Focus::focus_pw<T_out, imode><<<G, B, 0, this->stream>>>(
            tex[f], ppos, npix, vpos, nxmits_out, rpos, nelems_out, fs, fc,
            1.f / c0, nxmits_sum, nelems_sum, txfn, rxfn, t0, d_out, isBBIn,
            isBBOut);
        getLastCudaError("Focusing kernel failed.\n");
      }
    } else if (tdims[0] == 3) {
      this->logdebug("Executing virtual source focusing.");
      // It is more convenient to pass (x,y,z) coordinates as float3's.
      auto *vpos = reinterpret_cast<float3 *>(txdat.data());
      // Loop through frames
      this->out->resetToZeros();
      for (int f = 0; f < nframes; f++) {
        this->logdebug("Focusing frame {} of {}.", f + 1, nframes);
        T_out *d_out = this->out->data() + f * framesize;
        kernels::Focus::focus_vs<T_out, imode><<<G, B, 0, this->stream>>>(
            tex[f], ppos, npix, vpos, nxmits_out, rpos, nelems_out, fs, fc,
            1.f / c0, nxmits_sum, nelems_sum, txfn, rxfn, t0, d_out, isBBIn,
            isBBOut);
        getLastCudaError("Focusing kernel failed.\n");
      }
    } else if (tdims[0] == 4) {
      this->logdebug("Executing virtual source focusing.");
      // It is more convenient to pass (x,y,z,a) coordinates as float4's.
      auto *vpos = reinterpret_cast<float4 *>(txdat.data());
      // Loop through frames
      this->out->resetToZeros();
      for (int f = 0; f < nframes; f++) {
        this->logdebug("Focusing frame {} of {}.", f + 1, nframes);
        T_out *d_out = this->out->data() + f * framesize;
        kernels::Focus::focus_vs<T_out, imode><<<G, B, 0, this->stream>>>(
            tex[f], ppos, npix, vpos, nxmits_out, rpos, nelems_out, fs, fc,
            1.f / c0, nxmits_sum, nelems_sum, txfn, rxfn, t0, d_out, isBBIn,
            isBBOut);
        getLastCudaError("Focusing kernel failed.\n");
      }
    }
  }

  // else if (!txdat.empty() && txdat.getDimensions()[0] == 3) {
  // }
  // else if (!txdat.empty() && txdat.getDimensions()[0] == 2) {
  //   this->logdebug("Executing plane wave focusing.");
  //   // It is more convenient to pass (theta, phi) coordinates as float2's.
  //   auto *vpos = reinterpret_cast<float2 *>(txdat.data());
  //   // Loop through frames
  //   this->out->resetToZeros();
  //   for (int f = 0; f < nframes; f++) {
  //     this->logdebug("Focusing frame {} of {}.", f + 1, nframes);
  //     T_out *d_out = this->out->data() + f * framesize;

  //     kernels::Focus::focus_pw<T_out, imode><<<G, B, 0, this->stream>>>(
  //         tex[f], ppos, npix, vpos, nxmits_out, rpos, nelems_out, fs, fc,
  //         1.f / c0, nxmits_sum, nelems_sum, txfn, rxfn, t0, d_out, isBBIn,
  //         isBBOut);
  //     getLastCudaError("Focusing kernel failed.\n");
  //   }
  // }
}

///////////////////////////////////////////////////////////////////////////
// Kernels
///////////////////////////////////////////////////////////////////////////
__device__ float3 kernels::Focus::rotate_xz(float3 vec, float theta) {
  float sint = sinf(theta);
  float cost = cosf(theta);
  return make_float3(vec.x * cost - vec.z * sint, vec.y,
                     vec.x * sint + vec.z * cost);
}
__device__ float3 kernels::Focus::compute_vec(float3 pos1, float3 pos2) {
  return pos2 - pos1;
}
__device__ float3 kernels::Focus::compute_vec(float4 pos1, float3 pos2) {
  return rotate_xz(pos2 - make_float3(pos1.x, pos1.y, pos1.z), pos1.w);
}
__device__ float3 kernels::Focus::compute_vec(float3 pos1, float4 pos2) {
  return rotate_xz(make_float3(pos2.x, pos2.y, pos2.z) - pos1, pos2.w);
}

__device__ float kernels::Focus::compute_fnum(float3 vec, float pad) {
  // Use a bit of padding to give finite width to focal depth
  return (pad + fabsf(vec.z)) * rnorm3df(vec.x, vec.y, 0.f) * 0.5f;
}
__device__ float kernels::Focus::compute_tof(float3 vec, float c0inv) {
  return norm3df(vec.x, vec.y, vec.z) * c0inv;
}

// Core kernel with template options for conditional execution. Templatizing
// the kernel allows the compiler to make branch-wise optimizations at
// compile-time. That is, the GPU will not actually go through the if/else
// branches for template parameters at runtime, and will instead execute the
// correct compiled version for each branch.
template <typename T_out, IM imode, typename XYZA_tx, typename XYZA_rx>
__global__ void kernels::Focus::focus_bd(cudaTextureObject_t tex, float3 *ppos,
                                         int npx, XYZA_tx *tpos, int ntxo,
                                         XYZA_rx *rpos, int nrxo, float fs,
                                         float fd, float c0inv, int nrxsum,
                                         float txfn, float rxfn, float *t0,
                                         T_out *out, bool bb_input,
                                         bool bb_output) {
  // Determine information about the current thread
  int poidx = blockIdx.x * blockDim.x + threadIdx.x;  // pixel output index
  int toidx = blockIdx.y * blockDim.y + threadIdx.y;  // transmit output index
  int roidx = blockIdx.z * blockDim.z + threadIdx.z;  // receive output index
  // Only compute if valid output index
  if (poidx < npx && toidx < ntxo && roidx < nrxo) {
    // Pre-compute the output sample index
    int oidx = poidx + npx * (toidx + ntxo * roidx);
    float3 pp = ppos[poidx + npx * toidx];  // Get the pixel coordinates
    T_out sum(0);                           // Initialize running sum
    float fnum_pad = 1.f / (fd * c0inv);    // 1 wavelength padding
    // Relative position of pixel with respect to transmit focus
    float3 pt = compute_vec(tpos[toidx], pp);
    float fnum_pt = compute_fnum(pt, fnum_pad);
    if (fnum_pt >= txfn) {  // Only proceed if tx/px f-number is valid
      // Get the signed time-of-flight from transmit focus to pixel
      float delt = copysign(compute_tof(pt, c0inv), pt.z) + t0[toidx];
      for (int ridx = roidx * nrxsum; ridx < (roidx + 1) * nrxsum; ridx++) {
        // Relative position of receive element with respect to pixel
        float3 rp = compute_vec(pp, rpos[ridx]);
        float fnum_rp = compute_fnum(rp, fnum_pad);
        if (fnum_rp >= rxfn) {  // If this rx/px pair has valid f-number
          // Get the delay for [vx -> px -> rx]
          float tau = delt + compute_tof(rp, c0inv);  // delay in time
          float sample = tau * fs;  // in samples (interpolation coordinate)
          int line = toidx + ntxo * ridx;  // texture line index
          // Interpolate
          T_out data = interp1d<T_out, imode>(tex, sample, line);
          // If input is baseband data, apply an additional phase rotation
          if constexpr (is_complex<T_out>()) {  // To avoid compiler errors
            if (bb_input) data *= expj2pi(-tau * fd);
          }
          sum += data;  // Accumulate data to synthesize the apertures
        }               // End if fnum_rp >= rxfn
      }                 // End loop over receive elements
    }                   // End if fnum_tp >= txfn
    // Make the output baseband if requested
    if constexpr (is_complex<T_out>()) {  // To avoid compiler errors
      if (bb_output) sum *= expj2pi(-pp.z * c0inv * fd);
    }
    out[oidx] = sum;
  }  // End if valid block
}

template <typename T_out, IM imode, typename XYZA_vx, typename XYZA_rx>
__global__ void kernels::Focus::focus_vs(cudaTextureObject_t tex, float3 *ppos,
                                         int npx, XYZA_vx *vpos, int nvxo,
                                         XYZA_rx *rpos, int nrxo, float fs,
                                         float fd, float c0inv, int nvxsum,
                                         int nrxsum, float txfn, float rxfn,
                                         float *t0, T_out *out, bool bb_input,
                                         bool bb_output) {
  // Determine information about the current thread
  int poidx = blockIdx.x * blockDim.x + threadIdx.x;  // pixel output index
  int voidx = blockIdx.y * blockDim.y + threadIdx.y;  // virt src output index
  int roidx = blockIdx.z * blockDim.z + threadIdx.z;  // receive output index
  // Only compute if valid output index
  if (poidx < npx && voidx < nvxo && roidx < nrxo) {
    // Pre-compute the output sample index
    int oidx = poidx + npx * (voidx + nvxo * roidx);
    float3 pp = ppos[poidx];              // Get the x, y, z pixel coordinates
    T_out sum(0);                         // Initialize running sum
    float sumcount = 0.f;                 // Also keep track of how many sums
    float fnum_pad = 1.f / (fd * c0inv);  // 1 wavelength padding
    // Loop through virtual sources to be summed
    for (int vidx = voidx * nvxsum; vidx < (voidx + 1) * nvxsum; vidx++) {
      // Relative position of pixel with respect to virtual source
      float3 pv = compute_vec(vpos[vidx], pp);
      float fnum_pv = compute_fnum(pv, fnum_pad);
      if (fnum_pv >= txfn) {  // Only proceed if vx/px f-number is valid
        // Get the signed time-of-flight from virtual source to pixel
        float delv = copysignf(compute_tof(pv, c0inv), pv.z) + t0[vidx];
        // Check whether rx/px pair falls within f-number range
        for (int ridx = roidx * nrxsum; ridx < (roidx + 1) * nrxsum; ridx++) {
          float3 rp = compute_vec(pp, rpos[ridx]);
          float fnum_rp = compute_fnum(rp, fnum_pad);
          if (fnum_rp >= rxfn) {  // If this rx/px pair has valid f-number
            // Get the delay for [vx -> px -> rx]
            float tau = delv + compute_tof(rp, c0inv);  // delay in time
            float sample = tau * fs;  // in samples (interpolation coordinate)
            int line = vidx + nvxsum * nvxo * ridx;  // texture line index
            // Interpolate
            T_out data = interp1d<T_out, imode>(tex, sample, line);
            // If input is baseband data, apply an additional phase rotation
            if constexpr (is_complex<T_out>()) {  // To avoid compiler errors
              if (bb_input) data *= expj2pi(-tau * fd);
            }
            sum += data;      // Accumulate data to synthesize the apertures
            sumcount += 1.f;  // Keep track of sums to take the mean later
          }                   // End if fnum_rp >= rxfn
        }                     // End loop over receive elements
      }                       // End if fnum_vp >= txfn
    }                         // End loop over virtual sources
    // Make the output baseband if requested
    if constexpr (is_complex<T_out>()) {  // To avoid compiler errors
      if (bb_output) sum *= expj2pi(-pp.z * c0inv * fd);
    }
    if (sumcount > 0) out[oidx] = sum / sumcount;
  }  // End if valid block
}

template <typename T_out, IM imode, typename XYZA_rx>
__global__ void kernels::Focus::focus_pw(cudaTextureObject_t tex, float3 *ppos,
                                         int npx, float2 *tdir, int ntxo,
                                         XYZA_rx *rpos, int nrxo, float fs,
                                         float fd, float c0inv, int ntxsum,
                                         int nrxsum, float txfn, float rxfn,
                                         float *t0, T_out *out, bool bb_input,
                                         bool bb_output) {
  // Determine information about the current thread
  int poidx = blockIdx.x * blockDim.x + threadIdx.x;  // pixel output index
  int toidx = blockIdx.y * blockDim.y + threadIdx.y;  // transmit output index
  int roidx = blockIdx.z * blockDim.z + threadIdx.z;  // receive output index
  // Only compute if valid output index
  if (poidx < npx && toidx < ntxo && roidx < nrxo) {
    // Pre-compute the output sample index
    int oidx = poidx + npx * (toidx + ntxo * roidx);
    float3 pp = ppos[poidx];              // Get the x, y, z pixel coordinates
    T_out sum(0);                         // Initialize running sum
    float sumcount = 0.f;                 // Also keep track of how many sums
    float fnum_pad = 1.f / (fd * c0inv);  // 1 wavelength padding
    // Loop through transmit plane waves to be summed
    for (int tidx = toidx * ntxsum; tidx < (toidx + 1) * ntxsum; tidx++) {
      // Get the sines and cosines of the plane wave direction
      auto [az, el] = tdir[tidx];  // Azimuth, elevation angles
      // Explicitly use the faster __sincosf() intrinsic (max error ~1e-7).
      float sinaz, cosaz, sinel, cosel;
      __sincosf(az, &sinaz, &cosaz);
      __sincosf(el, &sinel, &cosel);
      // Only proceed if: fnum_tx = cos(a)/sin(a) >= txfn
      if (cosaz >= txfn * sinaz && cosel >= txfn * sinel) {
        // Get the time-of-flight of plane wave to each pixel
        float delt = sinaz * (pp.x * cosel + pp.y * sinel) + cosaz * pp.z;
        delt = (delt * c0inv) + t0[tidx];  // Convert to seconds and add t0
        // Loop through receive elements to be summed
        for (int ridx = roidx * nrxsum; ridx < (roidx + 1) * nrxsum; ridx++) {
          float3 rp = compute_vec(pp, rpos[ridx]);
          float fnum_rp = compute_fnum(rp, fnum_pad);
          if (fnum_rp >= rxfn) {  // If this rx/px pair has valid f-number
            // Get the total delay for [tx -> px -> rx]
            float tau = delt + compute_tof(rp, c0inv);  // in time
            float sample = tau * fs;  // in samples (interpolation coordinate)
            int line = tidx + ntxsum * ntxo * ridx;  // texture line index
            // Interpolate using imode (e.g., CUBIC, LINEAR)
            T_out data = interp1d<T_out, imode>(tex, sample, line);
            // If input is baseband data, apply an additional phase rotation
            if constexpr (is_complex<T_out>()) {  // To avoid compiler errors
              if (bb_input) data *= expj2pi(-tau * fd);
            }
            sum += data;      // Accumulate data to synthesize the apertures
            sumcount += 1.f;  // Keep track of sums to take the mean later
          }                   // End if fnum_rp >= rxfn
        }                     // End loop over receive elements
      }                       // End if fnum_tx >= txfn
    }                         // End loop over transmits
    // Make the output baseband if requested
    if constexpr (is_complex<T_out>()) {  // To avoid compiler errors
      if (bb_output) sum *= expj2pi(-pp.z * c0inv * fd);
    }
    // Store result
    if (sumcount > 0) out[oidx] = sum / sumcount;
  }  // End if thread is valid
}

template class Focus<short, float>;
template class Focus<float, float>;
template class Focus<cuda::std::complex<short>, cuda::std::complex<float>>;
template class Focus<cuda::std::complex<float>, cuda::std::complex<float>>;

}  // namespace rtbf
