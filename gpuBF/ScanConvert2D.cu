/**
 @file gpuBF/ScanConvert2D.cu
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2022-08-18

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "ScanConvert2D.cuh"

namespace rtbf {

template <typename T_in, typename T_out>
ScanConvert2D<T_in, T_out>::ScanConvert2D(
    std::shared_ptr<Tensor<T_in>> input, const Tensor<float> *h_depths,
    const Tensor<float> *h_angles, const Tensor<float> *pixelPositions,
    ScanConvert2DMode scmode, float apexDist, cudaStream_t cudaStream,
    std ::string moniker, std::string loggerName) {
  this->label = moniker;
  this->setLogger(loggerName);
  this->logdebug("Initializing {}.", this->label);

  // Read inputs
  this->in = input;                 // Share ownership of input Tensor
  this->stream = cudaStream;        // Set asynchronous stream
  gpuID = this->in->getDeviceID();  // Execute on input's device
  mode = scmode;                    // Scan conversion mode
  apex = apexDist;                  // Distance to apex

  // Check input dimensions
  auto idims = this->in->getDimensions();  // Get dimensions of input
  size_t nr = idims[0];
  size_t na = idims[1];
  nframes = prod(idims) / nr / na;
  if (h_depths->getNumberOfValidElements() != nr)
    this->template logerror<std::runtime_error>(
        "Expected depths to have length {} for data with size {}. Found {}.",
        nr, vecString(idims), h_depths->getNumberOfValidElements());
  if (h_angles->getNumberOfValidElements() != na)
    this->template logerror<std::runtime_error>(
        "Expected angles to have length {} for data with size {}. Found {}.",
        na, vecString(idims), h_angles->getNumberOfValidElements());
  auto pdims = pixelPositions->getDimensions();
  if (pdims.size() < 2 || pdims[0] != 3)
    this->template logerror<std::runtime_error>(
        "Expected pixel positions to have dimensions [3, ...]. Found {}.",
        vecString(pdims));
  if (mode == ScanConvert2DMode::SECTOR && apex != 0) {
    this->logwarn(
        "Requested 'SECTOR' scan conversion but provided non-zero apex {}. "
        "Ignoring apex.",
        apex);
    apex = 0.f;
  }
  if (mode != ScanConvert2DMode::SECTOR &&
      mode != ScanConvert2DMode::CURVILINEAR &&
      mode != ScanConvert2DMode::PHASED)
    this->template logerror<std::runtime_error>(
        "Found unknown ScanConvert2DMode {}.", mode);

  // Copy data information to GPU
  r = Tensor<float>(*h_depths, gpuID, this->label + "->r", this->logName);
  a = Tensor<float>(*h_angles, gpuID, this->label + "->a", this->logName);
  pxpos = Tensor<float>(*pixelPositions, gpuID, this->label + "->pxpos",
                        this->logName);

  // Must run on the GPU
  if (gpuID < 0)
    this->template logerror<NotImplemented>(
        "No CPU implementation of ScanConvert2D yet.");

  // Important! ScanConvert2D uses textures, which have special alignment
  // requirements.
  std::vector<size_t> pitchdims = {nr, na};
  in_pitched = std::make_shared<Tensor<T_in>>(
      pitchdims, gpuID, this->label + "pitched", this->logName, true);

  // Initialize output array and other private arrays
  std::vector<size_t> odims(pdims.begin() + 1, pdims.end());  // Skip the "xyz"
  while (odims.size() < 2) odims.push_back(1);
  npixels = prod(odims);
  for (int d = 2; d < idims.size(); d++) odims.push_back(idims[d]);
  this->out = std::make_shared<Tensor<T_out>>(
      odims, gpuID, this->label + "->out", this->logName);
  ri = Tensor<float>(odims, gpuID, this->label + "->ri", this->logName);
  ai = Tensor<float>(odims, gpuID, this->label + "->ai", this->logName);

  // Set CUDA device
  CCE(cudaSetDevice(gpuID));
  initTextureObject();

  // Pre-compute the scan conversion lookup table
  auto getG = [](int b, int n) { return (n - 1) / b + 1; };
  dim3 blk(256);
  dim3 grd(getG(blk.x, npixels));
  kernels::ScanConvert2D::computeLUT<<<grd, blk, 0, this->stream>>>(
      r.data(), nr, a.data(), na, reinterpret_cast<float3 *>(pxpos.data()),
      ri.data(), ai.data(), npixels, mode, apex);
}

template <typename T_in, typename T_out>
ScanConvert2D<T_in, T_out>::~ScanConvert2D() {
  this->loginfo("Destroyed {}.", this->label);
}
template <typename T_in, typename T_out>
void ScanConvert2D<T_in, T_out>::initTextureObject() {
  if (gpuID < 0) return;
  // Set up textures to use the built-in bilinear interpolation hardware
  // Use texture objects (CC >= 3.0)
  // Texture description
  memset(&texDesc, 0, sizeof(texDesc));
  texDesc.addressMode[0] = cudaAddressModeBorder;
  texDesc.addressMode[1] = cudaAddressModeBorder;
  texDesc.filterMode = cudaFilterModeLinear;
  texDesc.normalizedCoords = 0;
  // Read mode depends on input and output types, determined at compile time.
  if constexpr (std::is_same_v<T_in, T_out>) {
    texDesc.readMode = cudaReadModeElementType;
  } else {
    static_assert(std::is_same_v<T_out, float> ||
                      std::is_same_v<T_out, cuda::std::complex<float>>,
                  "T_out must equal T_in or be floating point.");
    texDesc.readMode = cudaReadModeNormalizedFloat;
  }
  // Resource description
  std::vector<size_t> d = in_pitched->getDimensions();

  CCE(cudaSetDevice(gpuID));
  if (tex != 0) CCE(cudaDestroyTextureObject(tex));
  memset(&resDesc, 0, sizeof(resDesc));
  resDesc.resType = cudaResourceTypePitch2D;
  resDesc.res.pitch2D.width = d[0];
  resDesc.res.pitch2D.height = d[1];
  resDesc.res.pitch2D.desc =
      cudaCreateChannelDesc<typename TexType<T_in>::type>();
  resDesc.res.pitch2D.devPtr = in_pitched->data();
  resDesc.res.pitch2D.pitchInBytes = in_pitched->getPitchInBytes();
  this->logdebug(
      "Initializing CUDA texture object ({} x {}) from pitched memory at {}.",
      d[0], d[1], fmt::ptr(resDesc.res.pitch2D.devPtr));
  // Create texture object
  CCE(cudaCreateTextureObject(&tex, &resDesc, &texDesc, NULL));
}

template <typename T_in, typename T_out>
void ScanConvert2D<T_in, T_out>::convert() {
  auto idims = this->in->getDimensions();  // Get dimensions of input
  size_t nr = idims[0];
  size_t na = idims[1];
  // Apply scan conversion
  auto getG = [](int b, int n) { return (n - 1) / b + 1; };
  dim3 blk(256);
  dim3 grd(getG(blk.x, npixels));
  size_t istride = this->in->getPitch() * na;
  for (int f = 0; f < nframes; f++) {
    in_pitched->copyFromAsync(this->in->data() + f * istride, this->stream,
                              this->in->getPitchInBytes());
    kernels::ScanConvert2D::applyConversion<<<grd, blk, 0, this->stream>>>(
        tex, npixels, ri.data(), ai.data(), this->out->data() + f * npixels);
  }
}

///////////////////////////////////////////////////////////////////////////
// kernels
///////////////////////////////////////////////////////////////////////////
__global__ void kernels::ScanConvert2D::computeLUT(
    float *r, int nr, float *a, int na, float3 *pxpos, float *ri, float *ai,
    int npx, ScanConvert2DMode mode, float apex) {
  // Determine information about the current thread
  int pidx = blockIdx.x * blockDim.x + threadIdx.x;
  if (pidx < npx) {
    // Get the current pixel coordinates
    auto [px, py, pz] = pxpos[pidx];
    // Convert spherical to cartesian coordinates
    float pr = norm3df(px, 0, pz + apex);
    float pa = atan2f(px, pz + apex);
    if (mode == ScanConvert2DMode::CURVILINEAR) {
      pr -= apex;
    } else if (mode == ScanConvert2DMode::PHASED) {
      pr -= apex / cosf(pa);
    }

    // Further convert into the indices for faster sampling later
    // Extremely naive implementation, but this is outside of the real-time loop
    if (nr == 1) {
      ri[pidx] = 0;  // If only one r, use it
    } else {
      // Find i such that r[i-1] <= pr <= r[i] and find interp coord
      int i = 0;
      while (i < nr && pr >= r[i]) i++;
      if (i == 0)
        ri[pidx] = -1;
      else if (i == nr)
        ri[pidx] = nr;
      else
        ri[pidx] = i + (pr - r[i]) / (r[i] - r[i - 1]);
    }

    if (na == 1) {
      ai[pidx] = 0;
    } else {
      // Find j such that a[j-1] <= pa <= a[j] and find interp coord
      int j = 0;
      while (j < na && pa >= a[j]) j++;
      if (j == 0)
        ai[pidx] = -1;
      else if (j == na)
        ai[pidx] = na;
      else
        ai[pidx] = j + (pa - a[j]) / (a[j] - a[j - 1]);
    }
  }
}
template <typename T_out>
__global__ void kernels::ScanConvert2D::applyConversion(cudaTextureObject_t tex,
                                                        int npx, float *ri,
                                                        float *ai, T_out *out) {
  // Determine information about the current thread
  int pidx = blockIdx.x * blockDim.x + threadIdx.x;
  if (pidx < npx) {
    float pri = ri[pidx];
    float pai = ai[pidx];
    if constexpr (is_complex<T_out>()) {
      out[pidx] = castToComplex(tex2D<float2>(tex, pri + 0.5f, pai + 0.5f));
    } else {
      out[pidx] = tex2D<T_out>(tex, pri + 0.5f, pai + 0.5f);
    }
  }
}

// TODO: Add support for other input, output data types.
template class ScanConvert2D<float, float>;
template class ScanConvert2D<cuda::std::complex<float>,
                             cuda::std::complex<float>>;
}  // namespace rtbf