/**
 @file gpuBF/Demodulate.cuh
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2022-08-05

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef RTBF_DEMODULATE_CUH_
#define RTBF_DEMODULATE_CUH_

#include "Hilbert.cuh"
#include "Operator.cuh"

namespace rtbf {

/**	@brief Class to demodulate signals.

This class takes modulated data and demodulates it at a given frequency. It can
also be used to remodulate data by "demodulating" at a negative frequency.
Demodulate can be performed out-of-place (default) or in-place to save memory.

If a real RF signal is provided, Demodulate will first apply a Hilbert transform
to obtain the analytic RF signal. Subsequent demodulation will be performed
in-place on the Hilbert transform output, which is still out-of-place with
respect to the original input.

*/
template <typename T_in, typename T_out>
class Demodulate : public Operator<Tensor<T_in>, Tensor<T_out>> {
 private:
  int gpuID;                  ///< Device to execute on
  Tensor<float> times;        ///< Time array (with optional axis broadcasting)
  float fdemod;               ///< Demodulation frequency
  std::vector<size_t> idims;  ///< Input Tensor dimensions
  std::vector<size_t> tdims;  ///< Time array Tensor dimensions
  std::vector<size_t> odims;  ///< Output Tensor dimensions
  bool inplace;               ///< Whether to perform in place
  dim3 id3;                   ///< Input Tensor dimensions represented as dim3
  dim3 td3;        ///< Time array Tensor dimensions represented as dim3
  dim3 od3;        ///< Output Tensor dimensions represented as dim3
  size_t nframes;  ///< Number of input frames
  size_t tframes;  ///< Number of time array frames
  size_t oframes;  ///< Number of time array frames

 public:
  Demodulate(std::shared_ptr<Tensor<T_in>> input, const Tensor<float> *times,
             float freqDemod, bool performInPlace = false,
             cudaStream_t cudaStream = 0, std ::string moniker = "Demodulate",
             std::string loggerName = "");
  virtual ~Demodulate();

  /// @brief Sum channels together
  void demodulate();
};

namespace DemodulateKernels {

template <typename T_in, typename T_out>
void h_demod(T_in *idata, std::vector<size_t> idims, size_t ipitch,
             float *tdata, std::vector<size_t> tdims, size_t tpitch,
             T_out *odata, std::vector<size_t> odims, size_t opitch, float fd);

template <typename T_in, typename T_out>
__global__ void demod(T_in *idata, dim3 idims, int ipitch, float *tdata,
                      dim3 tdims, int tpitch, T_out *odata, dim3 odims,
                      int opitch, float fd);
}  // namespace DemodulateKernels

template <typename>
struct is_Demodulate : std::false_type {};
template <typename T_in, typename T_out>
struct is_Demodulate<Demodulate<T_in, T_out>> : std::true_type {};
}  // namespace rtbf

#endif /* RTBF_DEMODULATE_CUH_ */
