# rtbf - Real-time beamforming for ultrasound using GPUs

`rtbf` is a high-throughput low-latency ultrasound software beamformer that uses graphics processing units (GPUs) to achieve real-time imaging.

## Announcements

[2022-08-25] A scan conversion utility [ScanConvert2D](gpuBF/ScanConvert2D.cuh) has been added. Supported modes include the most common `'SECTOR'`, `'CURVILINEAR'`, and `'PHASED'` scan formats. Call `rtbf_mex help ScanConvert2D` for more information.

Compiled MEX functions are now available on the [Releases](https://gitlab.com/dongwoon.hyun/rtbf/-/releases) page!

Please check the website often for updates. See the [changelog](CHANGELOG.md) for the latest changes and updates to the repository.

## Table of Contents

1. [Example usage](#example-usage)
2. [Installation instructions](#installation-instructions)
3. [Motivation](#motivation)
4. [Code structure](#code-structure)
5. [Citing this work](#citing-this-work)

## Example Usage

### MATLAB Interface

Let `rfdata` be a RF data array with dimensions `[nsamps, nxmits, nelems]`, with int16 datatype. We define the beamforming operations using MATLAB structs.

#### Example 1: RF beamforming

```matlab
% Specify imaging configuration
F.operator = 'Focus';       % Focusing operation
F.pxpos = pixelPositions;   % - [3, {nD pixel array}], in m
F.elpos = elementPositions; % - [3, nelems], in m
F.c0 = soundspeed;          % - scalar, in m/s
F.fs = samplingFrequency;   % - scalar, in Hz
F.fc = centerFrequency;     % - scalar, in Hz
F.txbf_mode = 'SUM';        % - Do virtual source STA
F.rxbf_mode = 'IDENTITY';   % - Preserve RX channel dimension
F.txfnum = 2;               % - Virtual source STA f-number
F.rxfnum = 1;               % - Receive aperture f-number
C.operator = 'ChannelSum';  % Channel sum operation
C.axis = -1;                % - Axis along which to sum
H.operator = 'Hilbert';     % Hilbert transform operation
B.operator = 'Bmode';       % Envelope detection operation
B.compression = 'log';      % - Apply logarithmic compression
% Set up execution graph and execute on rfdata
bimg1 = rtbf_mex(rfdata, F, C, H, B);  % Focus->ChannelSum->Hilbert->Bmode
% Re-use the graph on new rfdata
bimg1 = rtbf_mex(rfdata);
```

#### Example 2: IQ beamforming

```matlab
% This time, apply the Hilbert transform first and do IQ beamforming
% Hilbert->Focus->ChannelSum->Bmode
bimg2 = rtbf_mex('alt_graph', rfdata, H, F, C, B);  % Also specify graph name
bimg2 = rtbf_mex('alt_graph', rfdata);              % Re-use 'alt_graph'
bimg1 = rtbf_mex(rfdata);                           % Re-use the default graph
% Clear the CPU and GPU memory stored by all graphs
clear rtbf_mex
```

More complex computational graphs are possible by explicitly naming the input and output tensors for each struct. Refer to the [examples](examples/) to see example usage in a MATLAB Live notebook!

For more information on each operator, use the built-in help documentation.

```matlab
rtbf_mex('help')
```

## Installation Instructions

### Pre-compiled MATLAB MEX functions

For the basic functionality of `rtbf` (delay-and-sum, STA, etc.), pre-compiled MATLAB MEX functions are provided on the [Releases](https://gitlab.com/dongwoon.hyun/rtbf/-/releases) page for Windows and Linux.

### Custom code

To create and deploy custom GPU-accelerated code (e.g., a new [Operator](gpuBF/Operator.cuh)), you will need to compile `rtbf` yourself. Detailed step-by-step installation instructions for Linux and Windows are provided in [INSTALL.md](INSTALL.md).

## Motivation

Ultrasound scanners produce large amounts of raw data that are processed into images. In the past, *application-specific hardware* was used to process and downsample the data into more manageable forms (e.g., apply time delays and sum channels). With recent technological advances, it is now feasible to perform these operations entirely in software at real-time rates using graphics processing units (GPUs). The goal of `rtbf` is to accelerate software beamforming to enable rapid prototyping of experimental methods and to increase the accessibility of GPU computing for ultrasound researchers.

![images/phantom2.gif](images/phantom2.gif)

Examples of real-time imaging with `rtbf` using a custom [speckle-reducing neural network beamformer](https://gitlab.com/dongwoon.hyun/nn_bmode).

## Code structure

### `gpuBF`

This repository consists of a central library called [`gpuBF`](gpuBF/), a pure CUDA C/C++ library of beamforming classes structured around two main classes: [`Tensor`](gpuBF/Tensor.cuh) and [`Operator`](gpuBF/Operator.cuh).

[`Tensor<T>`](gpuBF/Tensor.cuh) is a class template that encapsulates a pointer to an `n`-dimensional array of type `T` in CPU or GPU memory, its descriptors (e.g., GPU index, pitch), as well as convenience functions for common operations like data transfer between the host and GPU device (e.g., `copyTo`, `copyFromAsync`). [`Tensor<T>`](gpuBF/Tensor.cuh)s are represented as `n`-dimensional arrays with arbitrary dimensions. The first dimension can be optionally pitched for more efficient memory access and compatibility with CUDA texture and surface objects.

[`Operator<T_in, T_out>`](gpuBF/Operator.cuh) is an abstract base class that accepts a shared pointer of type `T_in` (say, `Tensor<short>`) and produces a shared pointer of type `T_out` (say, `Tensor<cuda::std::complex<float>>`). [`Operator`](gpuBF/Operator.cuh)s can be assigned to specific CUDA streams to enable concurrent execution and/or multi-GPU processing. Some key examples of [`Operator`](gpuBF/Operator.cuh)s include:

* [`Hilbert`](gpuBF/Hilbert.cuh)
  Applies a Hilbert transform to the innermost (fastest-changing) dimension of an `n`-dimensional array, resulting in an analytic signal with the same dimensions as the input.
* [`Focus`](gpuBF/Focus.cuh)
  Applies transmit and receive beamforming based on the desired pixel locations, transducer element positions, sound speed, transmit and receive f-numbers, etc. This class can perform transmit aperture synthesis (virtual source and plane wave beamforming) as well as traditional "block-diagonal" beamforming, where each transmit corresponds to a disjoint set of image pixels (i.e. no transmit aperture synthesis). The user further specifies whether to sum or keep the transmit and receive array dimensions.
* [`FocusLUT`](gpuBF/FocusLUT.cuh)
  Similar to `Focus`, but allows the user to specify their own look-up tables for transmit and receive delays and apodizations.
* [`ChannelSum`](gpuBF/ChannelSum.cuh)
  Sums the channels into `N` equal subapertures. By default, `N = 1;`, i.e., all channels are summed into a single signal.
* [`Bmode`](gpuBF/Bmode.cuh)
  Applies envelope detection, and optionally, logarithmic or power compression. If desired, *incoherent compounding* can be applied along the last axis. This can be combined with `ChannelSum` to easily achieve receive spatial compounding.
* [`Refocus`](gpuBF/Refocus.cuh)
  Converts raw ultrasound data of size `[# samples, # tx events, # rx elems]` into multistatic data `[# samples, # tx elems, # rx elems]` using a user-supplied decoder matrix (specified as a field named `txinv`). This allows optimal retrospective transmit beamforming using **any** arbitrary transmit sequence.
* `ONNXGraphTRT`
  Uses NVIDIA's TensorRT framework to perform real-time inference of a saved ONNX neural network model. *NOTE: Conversion to v2.0.0 in progress.*
* [`VSXFormatter`](matBF/VSXFormatter.cuh)
  Provides a simplified interface to process raw receive buffer data from a Verasonics research system. The [`VSXFormatter`](matBF/VSXFormatter.cuh) unwraps, resamples, and reshapes the data for easy integration with the rest of `gpuBF`.
* In development: `ONNXGraphTRT`, `WallFilter`, `PowerDoppler` and `ColorDoppler`.

### `matBF`

`matBF` provides MATLAB interface called [`matBF`](matBF/) based on MATLAB's C++ Data API.

### `annBF`

(Coming soon.)
`annBF` consists of python implementations of artificial neural networks that are saved into a format that can be loaded into `gpuBF` via NVIDIA's TensorRT API. TensorRT is currently restricted to a small list of supported operations, but as this list expands, it will become possible to implement custom beamforming algorithms as neural networks (whether they include machine learning or not), with optimization performed via TensorRT rather than manual CUDA kernel tuning.

## Citing this work

If you find this code useful for any publications, please cite the following:

```bibtex
@inproceedings{hyun2019open,
  title={An open source {GPU}-based beamformer for real-time ultrasound imaging and applications},
  author={Hyun, Dongwoon and Li, You Leo and Steinberg, Idan and Jakovljevic, Marko and Klap, Tal and Dahl, Jeremy J},
  booktitle={2019 IEEE International Ultrasonics Symposium (IUS)},
  pages={20--23},
  year={2019},
  organization={IEEE}
}
```

For any work involving [Refocus](gpuBF/Refocus.cuh)-based beamforming, please additionally cite:

```bibtex
@inproceedings{hyun2021real,
  title={Real-Time Universal Synthetic Transmit Aperture Beamforming with Retrospective Encoding for Conventional Ultrasound Sequences ({REFoCUS})},
  author={Hyun, Dongwoon and Dahl, Jeremy J and Bottenus, Nick},
  booktitle={2021 IEEE International Ultrasonics Symposium (IUS)},
  pages={1--4},
  year={2021},
  organization={IEEE}
}
```
