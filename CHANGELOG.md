# Changelog

## [Version 2.0.7] as of 2022-08-25

### Added in v2.0.7

- A scan conversion utility [ScanConvert2D](gpuBF/ScanConvert2D.cuh) has been added. Supported modes include the most common `'SECTOR'`, `'CURVILINEAR'`, and `'PHASED'` scan formats. Call `rtbf_mex help ScanConvert2D` for more information.

## [Version 2.0.6] as of 2022-08-17

### Added in v2.0.6

- Help documentation has been added to `rtbf_mex`. Just type `rtbf_mex help` in MATLAB to see how to use each operator. If anything is unclear, please raise an issue and I will do my best to clarify!

### Changed in v2.0.6

- [SVDFilter](gpuBF/SVDFilter.cuh) has been modified to use the gesvdp algorithm instead of the gesvdr, which seems to run just as quickly as gesvdr but can return all singular values at once.

## [Version 2.0.5] as of 2022-08-17

### Added in v2.0.5

- [SVDFilter](gpuBF/SVDFilter.cuh) is a class that performs a reweighting of the singular value decomposition (SVD) of a dataset, and can be used for spatiotemporal clutter filtering for Doppler signals. [SVDFilter](gpuBF/SVDFilter.cuh) is implemented using randomized SVD. The user specifies the rank of the desired SVD and the weights to apply to the singular values. Optionally, the SVD can be performed in place (on the GPU) to save storage space. Other parameters for the randomized SVD include the number of iterations and the oversampling rate. In initial testing on an RTX 3090, a complex float Tensor of size [400, 300, 64], rank=20, niters=1, the total filtering time is approximately 43 ms. (Side note: cusolverDn's gesvdr seems to be ~50% faster than gesvda on this test case, which is why gesvdr was selected.)

## [Version 2.0.4] as of 2022-08-11

### Changed in v2.0.4

- [Refocus](gpuBF/Refocus.cuh) is significantly revamped to be more user-friendly. We now simply specify the transmit delays and apodizations to get refocused data. Tikhonov-regularized pseudoinversion is used by default, but users can specify their own encoder or decoder matrices if desired.

## [Version 2.0.3] as of 2022-08-08

### Added in v2.0.3

- [Demodulate](gpuBF/Demodulate.cuh) is a new Operator for demodulating signals. A demodulation frequency is specified, and a time vector is provided for the input data. Demodulation can be performed with respect to any axis via specification of the time vector. Any dimensions that are singleton are automatically broadcast.

- [Decimate](gpuBF/Decimate.cuh) is a new Operator to decimate signals (anti-alias filter and downsample). As it turns out, [Decimate](gpuBF/Decimate.cuh) is essentially the same operation as [ChannelSum](gpuBF/ChannelSum.cuh), although slightly more fundamental in nature. Thus [Decimate](gpuBF/Decimate.cuh) is implemented using the code from [ChannelSum](gpuBF/ChannelSum.cuh), with the specification of a "downsample factor" rather than a "number of output channels".

### Changed in v2.0.3

- [ChannelSum](gpuBF/ChannelSum.cuh)'s contents have been moved to [Decimate](gpuBF/Decimate.cuh), and now consists of a light wrapper around [Decimate](gpuBF/Decimate.cuh).

- [rtbf_mex](matBF/rtbf_mex.cuh) has been updated to include [Demodulate](gpuBF/Demodulate.cuh) and [Decimate](gpuBF/Decimate.cuh) and to suppress outputs by default.

## [Version 2.0.2] as of 2022-08-03

### Changes in v2.0.2

- [Focus](gpuBF/Focus.cuh) now supports inputs whose first dimension does not have pitch-alignment on the GPU, and now properly loops over the frame dimension(s) of inputs.
- The MATLAB MEX files are now conveniently available on the [Releases](https://gitlab.com/dongwoon.hyun/rtbf/-/releases) page.

## [Version 2.0.1] as of 2022-05-16

### Added in v2.0.1

- [Refocus](gpuBF/Refocus.cuh) has been added to the list of Operators. Refocus enables retrospective transmit beamforming by "decoding" the transmit sequence to recover multistatic data.
- An example of [Refocus](gpuBF/Refocus.cuh) usage is provided in [refocus_example.mlx](examples/refocus_example.mlx).

### Coming soon

- `ONNXGraphTRT`, `WallFilter`, `PowerDoppler` and `ColorDoppler` `Operator`s.
- MATLAB documentation for how to use `rtbf`.
- Example of how to write a new MATLAB MEX function based on [`rtbf_base.cuh`](matBF/rtbf_base.cuh).
- Example of how to use `rtbf_mex` with Field II simulated data and with Verasonics simulated data.

## [Version 2.0.0] as of 2022-05-06

Version 2.0.0 brings many breaking changes that vastly simplify the user interface and bring improved memory safety and code reliability.

### Added in v2.0.0

- A single, unified MATLAB interface that supersedes all previous implementations of `vsxBF`, which is now obsolete. This interface [rtbf_mex](matBF/rtbf_mex.cuh) allows the full specification of a computational graph in MATLAB without delving into C++ code. Error messages are caught by MATLAB's C++ MEX API and displayed in the Command Window directly.
- Improved logging is provided by `spdlog`, a dependency that is automatically downloaded during build. This library further includes the powerful `fmt` library for easy formatted printing and a wonderful escape from "chevron hell" (... << ... << ...).
- A MATLAB example of beamforming PICMUS data, illustrating many of the features of `rtbf_mex`.

### Changed in v2.0.0

- `DataArray` has been renamed to `Tensor`, and can now represent arbitrary dimensional arrays. Following the RAII idiom, the constructor is no longer decoupled from initialization, and the destructor automatically frees all internal heap-allocated memory as `Tensor`s go out of scope.
- `DataProcessor` has been renamed to `Operator`, templated by both its input and output types. Using C++11 smart pointers, `Operator`s now share ownership of their input and output to prevent dangling pointers to freed input or output `Tensor`s.
- The test functions have been updated to use the new interface.
- MATLAB MEX functions are now using the C++ API rather than the C API. The C++ API simplifies object lifetime management, catching exceptions via the MATLAB Command Window, and performing runtime datatype inference. Currently, the implementation does require an unsafe `const_cast` to access the raw read-only memory pointer to MATLAB `Array`s without inducing a deep copy.
- Most of the `vsxBF/` utilities, which actually depended only on MATLAB and not on VSX, have been moved to [`rtbf_base.cuh`](matBF/rtbf_base.cuh), with appropriate upgrades to the C++ API.
- `VSXDataFormatter` has been renamed to `VSXFormatter` and included in `matBF` directly. Those with MATLAB installed can use `VSXFormatter` to easily unwrap and format Verasonics data for live imaging or for offline processing.

### Removed in v2.0.0

- The entire `vsxBF/` folder has been removed, as it is no longer necessary to compile new C++ CUDA MEX files for each imaging configuration. Users are still free to write their own MATLAB MEX functions by inheriting from the [`rtbf_base.cuh`](matBF/rtbf_base.cuh) base class to create optimized applications.
- The `annBF/` folder is temporarily removed until its components can be brought up to speed with the rest of the toolbox.
- Any beamforming classes that are not yet upgraded have been removed temporarily. See below for what is in the pipeline for future releases.

### Coming soon

- MATLAB documentation for how to use `rtbf`.
- `Refocus`: An `Operator` that takes arbitrary transmit sequences and recovers multistatic synthetic transmit aperture data via the REFoCUS technique, which consists of a frequency-domain inversion of the transmitted delay profiles.
- `ONNXGraphTRT`: An `Operator` that accepts a vector of `Tensor<T>` inputs as well as a saved `.onnx` model, builds an optimized TensorRT engine, executes the engine, and outputs a vector of `Tensor<T>` results. Can be used to define new complex `Operator`s entirely in Python (e.g., via TensorFlow or PyTorch) and have them automatically optimized by TensorRT.
- `WallFilter`: A dedicated `Operator` for applying a high-pass filter to a Doppler ensemble of data to eliminate slow-moving targets. Depending on time and interest, possibly can include an `SVDFilter` for spatiotemporal clutter filtering.
- `PowerDoppler` and `ColorDoppler` `Operator`s to perform power estimation as well as phase-shift estimation.
- Example of how to write a new MATLAB MEX function based on [`rtbf_base.cuh`](matBF/rtbf_base.cuh).
- Example of how to use `rtbf_mex` with Field II simulated data and with Verasonics simulated data.

---

## [Version 1.0.2] as of 2021-07-22

Minor change to add utility function for loading from .bin files.

### Added in v1.0.2

- [vsxBF/functions/loadVSXBufferFromBinFile.m](vsxBF/functions/loadVSXBufferFromBinFile.m): Simple function to extract the saved binary data from the example scripts.

### Changed in v1.0.2

- Moved away from explicitly adding the subdirectories containing CMakeLists.txt for every transducer and imaging mode. Now, the vsxBF/CMakeLists.txt will search for vsxBF/*/*/CMakeLists.txt and add them automatically. This will reduce unnecessary conflicts when pulling in the master branch into private development branches.

### Removed in v1.0.2

- vsxBF/L12-3v/CMakeLists.txt: No longer needed.

## [Version 1.0.1] as of 2021-07-15

The L12-3v example scripts have been revamped to eliminate dead code and incorrect comments. Several new features have also been added to simplify Verasonics sequence programming (via the [vsxBF/helper_classes](vsxBF/helper_classes) wrapper classes), as well as utilities for acquiring anonymized data that is compliant with HIPAA regulations in the USA.

### Added in v1.0.1

- [vsxBF/helper_classes/ObjectArray.m](vsxBF/helper_classes/ObjectArray.m): A generic list of labeled objects. There are several convenient functions, such as `push` to add a new labeled object; `prevIdx`, `currIdx`, and `nextIdx` to quickly get the index of the previous, current, or next object in the list; and `get` to look up an object by its label.

- [vsxBF/helper_classes/SeqControlArray.m](vsxBF/helper_classes/SeqControlArray.m): An `ObjectArray` containing labeled `SeqControl` objects. Inherits all functions from `ObjectArray`, and additionally provides a `addSC` object to easily add a labeled `SeqControl` in a single line of code.

- [vsxBF/helper_classes/EventArray.m](vsxBF/helper_classes/EventArray.m): An `ObjectArray` containing labeled `Event` objects. Inherits all functions from `ObjectArray`, and additionally provides `addEvent`, `addTxRxFrame`, and `modifyPrevSeqControl` functions to handle common usage scenarios in a single line of code.

- [vsxBF/helper_classes/UIArray.m](vsxBF/helper_classes/UIArray.m): An `ObjectArray` containing labeled `UI` control objects. Inherits all functions from `ObjectArray`, and additionally provides functions to add sliders, buttons, etc. in a single line of code.

- [vsxBF/functions/generateFilename.m](vsxBF/functions/generateFilename.m): Function to automatically generate filenames in ascending order using either the current date and time, or using `cputime` for applications in which date and time must be anonymized (e.g., clinical acquisitions).

- [vsxBF/functions/scrambleTimestamps.m](vsxBF/functions/scrambleTimestamps.m): Function to scramble the last-written, creation, and last-accessed timestamps of a given file. Useful for applications in which date and time must be anonymized (e.g., clinical acquisitions).

### Changed in v1.0.1

- The example scripts in `vsxBF/L12-3v/*` have been updated to utilize the new features, as well as to fix comments and eliminate dead code.

- The new InterpMode was properly integrated into Focus.cu and Focus.cuh. Example scripts using the `Focus` class are forthcoming.

## [Version 1.0.0] as of 2021-05-27

This is the first "complete" major release of `rtbf`, with extensive testing on both Windows and Ubuntu systems. In particular, the CMake compilation is revamped to provide flexibility and compatibility. Also, we finally have our first [INSTALL.md](INSTALL.md) file that gives detailed step-by-step instructions on setting up the library.

### Added in v1.0.0

- [INSTALL.md](INSTALL.md): Installation instructions for Windows and Linux.
- CMake options, as opposed to inferring modules to build based on environment variables. These variables (`RTBF_BUILD_*`) are provided in the top-level [CMakeLists.txt](CMakeLists.txt). Hopefully, this will provide flexibility in the future.
- New git-friendly format for users to specify their compilation options to CMake directly (`-D` options on command line, or cache entries via GUI, or JSON entries for Visual Studio Code). See [INSTALL.md](INSTALL.md) for more details.
- New requirement/suggestion for the users to provide their own `CMAKE_CUDA_ARCHITECTURES` option. Rather than wasting time and space generating code for every single target GPU architecture, users should now specify their exact architecture (61, 70, 86, etc.) to generate optimized code. See [INSTALL.md](INSTALL.md) for more details.
- New standalone processing code for `vsxBF` SetUp scripts. In prior versions, separate scripts were necessary for real-time and offline processing. Now, the same scripts can be used for both, simplifying processing and improving reusability. See [vsxBF/L12-3v/bmode/process_L12_3v_bmode_synap.m](vsxBF/L12-3v/bmode/process_L12_3v_bmode_synap.m) for an example.
- To use TensorRT on Windows, a path to the Protobuf library must also be provided as the configuration option `LIBPROTOBUF_DIR`.

### Removed in v1.0.0

- `CMakeLists.txt.in`: The GoogleTest unit testing framework code was substantially simplified, so that this file was no longer necessary.

### Changed in v1.0.0

- Enforced CMake version >= 3.19. We found that earlier versions (<= 3.12) led to strange `-pthread` errors and other unexplained bugs that are fixed with 3.19. Earlier versions of CMake may work, but they have not been tested.
- The OBJECT library approach is replaced by STATIC libraries for `gpuBF`, `vsxBF_utils`, and `annBF_utils`. The former approach was not playing nicely with Windows and Visual Studio (required fiddling with options via manual point-and-click). The static library approach also substantially simplifies and cleans up dependencies in the downstream vsxBF CMakeLists.txt files.
- The TensorRT location should now be specified as an ordinary CMake variable `${TENSORRT_DIR}`. Previously, the TensorRT root directory was specified via an environment variable `$ENV{TENSORRT_DIR}` (a clumsy hack in retrospect). Now that CMake configuration options are being used extensively, it makes more sense to consolidate all variables in one location.

## [Unreleased] as of 2021-03-11

### Important instructions for updates from 2021-03-09

The `initialize()` function arguments for `FocusSynAp` have changed to include a specifier for the new `enum class` `InterpMode` as the third-to-last argument. The new signature is

```c++
void initialize(DataArray<T_in> *input, int nOutputRows, int nOutputCols,
                float outputSampsPerWL, float *h_delTx, float *h_delRx,
                float *h_apoTx = nullptr, float *h_apoRx = nullptr,
                float samplesPerCycle = 0.f,
                SynthesisMode synthMode = SynthesisMode::SynthTx,
                InterpMode intrpMode = InterpMode::Cubic,
                cudaStream_t cudaStream = 0, int verbosity = 1);
```

For example, any code that previously initialized a `FocusSynAp` object `F` as

```c++
F.initialize(&DF, nrows, ncols, outspwl, delTx, delRx, apoTx, apoRx, spcy,
             synMode, stream, verbosity);
```

should be updated to

```c++
F.initialize(&DF, nrows, ncols, outspwl, delTx, delRx, apoTx, apoRx, spcy,
             synMode, intMode, stream, verbosity);  // Added intMode!
```

Coming soon, these updates will receive a version number, rather than being named [Unreleased].

## [Unreleased] as of 2021-03-09

### Added

- This [changelog](CHANGELOG.md) to keep users aware of changes and upgrades to `rtbf`.
- 1D interpolation routines as device template [code](gpuBF/interp1d.cuh).
- New `enum class InterpMode` to select interpolation mode.
  - `InterpMode::Linear` Linear interpolation
  - `InterpMode::Cubic` Cubic Hermite spline interpolation
  - `InterpMode::Lanczos3` Lanczos 3-lobe interpolation
- [vsxBF_utils](vsxBF/utils/vsxBF_utils.cuh) was updated to allow user specification of interpolation mode.

### Modified

- [VSXDataFormatter](vsxBF/utils/VSXDataFormatter.cuh) class received a substantial upgrade.
  - Prior implementation used linear interpolation of Q components of IQ, which may have led to spectral artifacts.
  - New code applies a proper sinc-filtered resampling of the demodulated data for `NS200BW`, `BS100BW`, and `BS50BW` options of `VSXSampleMode`. (The `HILBERT` option is preserved to obtain modulated output.)
    - The resampling is computed using a 1D FFT, bandpass filtering, followed by a 1D IFFT.
    - The output is automatically decimated to eliminate redundant samples.
  - Template kernels are added to avoid unnecessary code duplication.
  - The `HilbertTransform` nested object is replaced by effectively the same kernels.
- [FocusSynAp](gpuBF/FocusSynAp.cuh) class kernels were rewritten.
  - Now utilizes a single template kernel whose branches are instantiated and optimized at compile-time.
    - Branches based on `InterpMode`, baseband vs. RF, and no apodization vs. apodization.
  - More scalable approach than having 12 separate kernels for every possible combination of branching.

### Removed

- Removed broken `L12_3v_mlbf` examples. These will be restored once they have been fixed.
