clear all
addpath ../matBF

img = single(rgb2gray(imread('peppers.png')));

SC.operator = 'ScanConvert2D';
SC.r = single((1:size(img,1))' / size(img,1) * 12e-3);
SC.theta = single(linspace(-36, 36, size(img,2))) * pi / 180;

dx = mean(diff(SC.r));  % Pixel spacing
dz = mean(diff(SC.r));  % Pixel spacing

%% First, do a 'SECTOR' scan (no apex)
SC.mode = 'SECTOR';
zmin = 0;
zmax = max(SC.r);
xmin = max(SC.r) * sin(min(SC.theta));
xmax = max(SC.r) * sin(max(SC.theta));
[SC.pxpos, sx, sz] = makeGrid(zmin, zmax, dz, xmin, xmax, dx);
simg = rtbf_mex(img, SC);

%% Next, do a 'CURVILINEAR' scan
SC.apex = 6e-3;
SC.mode = 'CURVILINEAR';
zmin = min(SC.r) - SC.apex * (1 - cos(max(abs(SC.theta))));
zmax = max(SC.r);
xmin = max(SC.r+SC.apex) * sin(min(SC.theta));
xmax = max(SC.r+SC.apex) * sin(max(SC.theta));
[SC.pxpos, cx, cz] = makeGrid(zmin, zmax, dz, xmin, xmax, dx);
cimg = rtbf_mex(img, SC);

%% Finally, do a 'PHASED' scan
SC.apex = 1.5e-3;
SC.mode = 'PHASED';
zmin = 0;
zmax = max(SC.r);
xmin = max(SC.r+SC.apex/cos(max(SC.theta))) * sin(min(SC.theta));
xmax = max(SC.r+SC.apex/cos(max(SC.theta))) * sin(max(SC.theta));
[SC.pxpos, px, pz] = makeGrid(zmin, zmax, dz, xmin, xmax, dx);
pimg = rtbf_mex(img, SC);

%% Plot all
figure(1); clf; colormap bone
subplot(221);imagesc(SC.theta, SC.r*1e3, img); xlabel('Angle (rad)'); ylabel('Depth (mm)')
subplot(222);imagesc(sx*1e3,sz*1e3,simg);axis image;xlabel('x (mm)');ylabel('z (mm)');title('Sector');
subplot(223);imagesc(cx*1e3,cz*1e3,cimg);axis image;xlabel('x (mm)');ylabel('z (mm)');title('Curvilinear');
subplot(224);imagesc(px*1e3,pz*1e3,pimg);axis image;xlabel('x (mm)');ylabel('z (mm)');title('Phased');


%% Subfunction to make grid
function [pxpos, x, z] = makeGrid(zmin, zmax, dz, xmin, xmax, dx)
    % Compute axes
    x = xmin:dx:xmax;
    z = zmin:dz:zmax;
    % Center axes about their limits
    x = x - mean(x) + mean([xmin xmax]);
    z = z - mean(z) + mean([zmin zmax]);
    % Create a grid
    [zz, xx] = ndgrid(z, x);
    pxpos = permute(cat(3, xx, 0*xx, zz), [3 1 2]);
end